package my.app.yrusanov.theplace.ui.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import my.app.yrusanov.theplace.R;

/**
 * Created by y.rusanov on 2017-4-2.
 */

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        ListPreference listPreference = (ListPreference) findPreference("list_preferences");
        listPreference.setSummary(listPreference.getEntry());
        listPreference.setPersistent(true);
        PreferenceManager.getDefaultSharedPreferences(this.getActivity().getBaseContext()).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("list_preferences")){
            Intent i = this.getActivity().getApplication().getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage(this.getActivity().getApplication().getBaseContext().getPackageName());
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        //else if(key.equals("switch_preference")){
        //    //ContentResolver.setMasterSyncAutomatically(sharedPreferences.getBoolean(key,true));
        //    Account newAccount = new Account(
        //            MainActivity.ACCOUNT, MainActivity.ACCOUNT_TYPE);
        //    ContentResolver.setSyncAutomatically(newAccount, MainActivity.AUTHORITY,sharedPreferences.getBoolean(key,true));
        //    Bundle settingsBundle = new Bundle();
        //
        //    settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        //    settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        //    ContentResolver.requestSync(newAccount, MainActivity.AUTHORITY, settingsBundle);
        //
        //}
    }
}