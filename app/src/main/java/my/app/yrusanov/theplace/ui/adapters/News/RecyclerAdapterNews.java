package my.app.yrusanov.theplace.ui.adapters.News;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.tables.News_table;
import my.app.yrusanov.image.ImagesFile;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2017-3-8.
 */

public class RecyclerAdapterNews extends RecyclerView.Adapter<RecyclerAdapterNews.ElementViewHolder> {

    ArrayList<News_table> elements;
    News_table newsTable;
    RecyclerAdapterNews.OnItemClickListener mItemClickListener;
    ContentResolver resolver;
    String path;

    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_NEWS+"");

    public RecyclerAdapterNews(Context _context, int _IdCategory){

        elements = new ArrayList<>();
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/"+ Types.NEWS+"";
        try {
            //elements = populateBeer.selectAllByCategory(_IdCategory);
            //elements = populateBeer.populateAll();
            if(_IdCategory != 0){
                resolver = _context.getContentResolver();
                String whereClause = ""+ Fields.IDNEWS+" = "+_IdCategory+"";
                Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
                if(cursor != null){
                    cursor.moveToFirst();
                    do {

                        Bitmap bitmap = ImagesFile.getImageByType(cursor.getString(1), Types.NEWS);
                        if (bitmap != null){
                            elements.add(new News_table(cursor.getInt(0), cursor.getString(1), cursor.getString(2),bitmap, cursor.getInt(4)));
                        }
                        else{
                            bitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.logo);
                            elements.add(new News_table(cursor.getInt(0), cursor.getString(1), cursor.getString(2),bitmap, cursor.getInt(4)));
                        }
                    } while (cursor.moveToNext());
                }

            }
        }
        catch(ArrayIndexOutOfBoundsException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public RecyclerAdapterNews.ElementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_news, parent, false);
        return new RecyclerAdapterNews.ElementViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapterNews.ElementViewHolder holder, int position) {
        if(elements != null) {
            newsTable = (News_table) elements.get(position);
            holder.textViewName.setText(newsTable.getName());
            holder.imageViewProfile.setImageBitmap(newsTable.getImage());
        }
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }


    public class ElementViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener
    {
        private final TextView textViewName;
        private ImageView imageViewProfile;

        public ElementViewHolder(View view){
            super(view);

            view.setOnClickListener(this);
            textViewName = (TextView) view.findViewById(R.id.textViewName_news);
            imageViewProfile = (ImageView)view.findViewById(R.id.imageView_news);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface  OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final RecyclerAdapterNews.OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }


}
