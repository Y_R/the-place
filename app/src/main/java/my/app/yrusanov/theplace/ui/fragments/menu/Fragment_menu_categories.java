package my.app.yrusanov.theplace.ui.fragments.menu;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.adapters.Menu.RecyclerAdapterMenuItems;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_menu_categories extends Fragment{

    RecyclerView recyclerView;
    //RecyclerAdapter_menu_item adapter;
    RecyclerAdapterMenuItems adapter;
    Fragment_menu_item fragment_menu_item;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    android.app.Fragment fragmentItem;
    View view;

    public Fragment_menu_categories() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_menu_categories, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_menu_items);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        //GridLayoutManager gridLayoutManager = new GridLayoutManager(view.getContext(),2);
        recyclerView.setLayoutManager(mLayoutManager);

        adapter = new RecyclerAdapterMenuItems(this.getActivity().getBaseContext(),this.getActivity().getIntent().getIntExtra("IdCategory",0));

        recyclerView.setAdapter(adapter);
        fragmentManager = getFragmentManager();
        fragment_menu_item = new Fragment_menu_item();
        adapter.SetOnItemClickListener(new RecyclerAdapterMenuItems.OnItemClickListener() {

            @Override
            public void OnItemClick(View view, int position) {
                TextView textView = (TextView) view.findViewById(R.id.textViewName_MenuFood);
                TextView textViewId = (TextView) view.findViewById(R.id.textViewID_MenuFood);
                Bundle bundle = new Bundle();
                bundle.putString("Name",textView.getText().toString());
                bundle.putInt("Id",Integer.valueOf(textViewId.getText().toString()));
                fragment_menu_item.setArguments(bundle);

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left,R.animator.enter_from_left, R.animator.exit_to_right);
                fragmentTransaction.replace(R.id.fragment_container,fragment_menu_item);
                fragmentTransaction.addToBackStack("fragment_menu_categories");

                fragmentTransaction.commit();
            }
        });

        return view;
    }

}
