package my.app.yrusanov.theplace.ui.tables;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-9-7.
 */
public class BeerCategory {

    private int ID;
    private String Name;
    private ArrayList<BeerCategory> beer_categoryArrayList;
    private Bitmap Image;

    public BeerCategory(int _ID, String _Name, Bitmap _image){
        this.ID = _ID;
        this.Name = _Name;
        this.Image = _image;
    }

    public BeerCategory(BeerCategory beer_category){
        this.ID = beer_category.getID();
        this.Name = beer_category.getName();
    }

    public BeerCategory(String _Name){
        this.Name = _Name;
    }

    public BeerCategory(String _Name, Bitmap _image){
        this.Name = _Name;
        this.Image = _image;
    }

    public BeerCategory(ArrayList<Object> beer_categoryArrayList){
        this.setBeer_categoryArrayList(beer_categoryArrayList);
    }

    public void setID(int _ID){
        this.ID = _ID;
    }

    public void setName(String _Name){
        this.Name = _Name;
    }

    public int getID(){
        return ID;
    }

    public String getName(){
        return Name;
    }

    public ArrayList<BeerCategory> getBeer_categoryArrayList() {
        return beer_categoryArrayList;
    }

    public void setBeer_categoryArrayList(ArrayList<?> beer_categoryArrayList) {

        ArrayList<BeerCategory> localBeerCategoryArrayList = new ArrayList<>();
        ArrayList<Object> localArrayList = new ArrayList<>();
        for(int i = 0; i < beer_categoryArrayList.size(); i++){
            localArrayList = (ArrayList<Object>) beer_categoryArrayList.get(i);

            localBeerCategoryArrayList.add(new BeerCategory(Integer.parseInt((String) localArrayList.get(0)), (String) localArrayList.get(1), (Bitmap)localArrayList.get(2)));
        }
        this.beer_categoryArrayList = localBeerCategoryArrayList;
    }

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }
}
