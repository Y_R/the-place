package my.app.yrusanov.theplace.ui.adapters.Beers;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.tables.Beer;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-8-11.
 */
public class RecyclerAdapterBeers extends RecyclerView.Adapter<RecyclerAdapterBeers.ElementViewHolder>{

    ArrayList<Beer> elements;
    Beer beer;
    OnItemClickListener mItemClickListener;
    ContentResolver resolver;
    String path;
    public String language;
    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_BEERS+"");

    public RecyclerAdapterBeers(Context _context, int _IdCategory){

        elements = new ArrayList<>();
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/Beer";
        String contentName;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(_context);
        language  = prefs.getString("list_preferences","en");
        try {
            //elements = populateBeer.selectAllByCategory(_IdCategory);
            //elements = populateBeer.populateAll();
            if(_IdCategory != 0){
                resolver = _context.getContentResolver();
                String whereClause = ""+ Fields.IDCATEGORY+" = "+_IdCategory+"";
                Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
                if(cursor != null){
                    cursor.moveToFirst();
                    do {
                        //System.out.println(cursor.getInt(7));
                        File imageFile;
                        imageFile = new File(path + "/"+cursor.getString(1)+".jpg");

                        contentName = DBOperation.getTranslation(_context, cursor.getInt(0), Types.BEER,language,"Name");
                        if(contentName.equals("")) {
                            contentName = cursor.getString(1);
                        }


                        if(imageFile.exists()){
                            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                            elements.add(new Beer(cursor.getInt(0), contentName, cursor.getString(3),bitmap, cursor.getFloat(6), cursor.getInt(7), cursor.getString(8), cursor.getString(9), cursor.getString(2)));
                        }
                        else{
                            Bitmap bitmap = BitmapFactory.decodeResource(_context.getResources(),R.drawable.beer);
                            elements.add(new Beer(cursor.getInt(0), contentName, cursor.getString(3),bitmap, cursor.getFloat(6), cursor.getInt(7), cursor.getString(8), cursor.getString(9), cursor.getString(2)));
                        }
                    } while (cursor.moveToNext());
                }

            }
        }
        catch(ArrayIndexOutOfBoundsException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public ElementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_beer, parent, false);
        return new ElementViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(ElementViewHolder holder, int position) {
        if(elements != null) {
            beer = (Beer) elements.get(position);
            holder.textViewName.setText(beer.getName());
            //holder.textViewAlcohol.setText(beer.getAlcohol());
            holder.textViewAlcohol.setText(beer.getAlcohol());
            //holder.textViewDescription.setText(beer.getDescription());
            holder.textViewCountry.setText(beer.getCountry());
            holder.textViewPrice.setText(Float.toString(beer.getPrice()));
            holder.textViewStyle.setText(beer.getStyle());
            holder.textViewWeight.setText(beer.getWeight());
            holder.imageViewProfile.setImageBitmap(beer.getImage());
            holder.textViewId.setText(String.valueOf(beer.getID()));
        }
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }


    public class ElementViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener
    {
        private final TextView textViewName;
        private final TextView textViewAlcohol;
        private final TextView textViewStyle;
        private final TextView textViewPrice;
        private final TextView textViewCountry;
        private final TextView textViewWeight;
        private final TextView textViewId;

        //private final TextView textViewDescription;
        private ImageView imageViewProfile;

        public ElementViewHolder(View view){
            super(view);

            view.setOnClickListener(this);
            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewAlcohol = (TextView) view.findViewById(R.id.textViewAlcohol_Beer);
            //textViewDescription = (TextView) view.findViewById(R.id.textViewDescription);
            textViewCountry = (TextView)view.findViewById(R.id.textViewCountry_Beer);
            textViewPrice = (TextView)view.findViewById(R.id.textViewPrice_Beer);
            textViewWeight = (TextView)view.findViewById(R.id.textViewWeight_Beer);
            textViewStyle = (TextView)view.findViewById(R.id.textViewStyle_Beer);
            textViewId = (TextView)view.findViewById(R.id.textViewID_Beer);

            imageViewProfile = (ImageView)view.findViewById(R.id.imageProfile);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface  OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }



}
