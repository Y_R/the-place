package my.app.yrusanov.theplace.ui.tables;

/**
 * Created by y.rusanov on 2016-9-27.
 */

public class MenuType {

    private int Id;
    private String Name;

    public MenuType(int id, String name) {
        Id = id;
        Name = name;
    }
    public MenuType(String name) {
        Name = name;
    }
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
