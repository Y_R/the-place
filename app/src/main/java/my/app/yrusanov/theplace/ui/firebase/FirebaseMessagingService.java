package my.app.yrusanov.theplace.ui.firebase;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.activities.MainActivity;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by y.rusanov on 2017-2-19.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {


    public static final String ACCOUNT = "BeerBox";
    public static final String AUTHORITY = "my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox";
    public static final String ACCOUNT_TYPE = "example.com";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().size() > 0) {

            String result = String.valueOf(remoteMessage.getData().get("message"));

            if(result.equals("reset")){
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Bundle settingsBundle = new Bundle();
                        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                        settingsBundle.putBoolean("reset", true);
                        ContentResolver.requestSync(new Account(
                                MainActivity.ACCOUNT, MainActivity.ACCOUNT_TYPE), MainActivity.AUTHORITY, settingsBundle);
                        showNotification("Reset");
                    }
                },2000);
            }
            else if(result.equals("delete")){
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Bundle settingsBundle = new Bundle();
                        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                        settingsBundle.putBoolean("delete", true);
                        ContentResolver.requestSync(new Account(
                                MainActivity.ACCOUNT, MainActivity.ACCOUNT_TYPE), MainActivity.AUTHORITY, settingsBundle);
                        showNotification("Delete");
                    }
                },2000);
            }
            else{
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Bundle settingsBundle = new Bundle();
                        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                        ContentResolver.requestSync(new Account(
                                MainActivity.ACCOUNT, MainActivity.ACCOUNT_TYPE), MainActivity.AUTHORITY, settingsBundle);
                        showNotification("Updated");
                    }
                },10000);
            }



        }
    }

    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
            */
            ContentResolver.requestSync(newAccount, AUTHORITY,null);
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
            Log.i("Account", "Exists");
        }
        return newAccount;
    }
    private void showNotification(String message) {
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_ONE_SHOT);

        Uri notSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder= new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("Beer Box")
                .setContentText(message)
                .setSound(notSound)
                .setSmallIcon(R.mipmap.ic_launcher_beerbox)
                //.setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                .setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0,builder.build());
    }
}
