package my.app.yrusanov.theplace.ui.server;

import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;

import org.firebirdsql.jdbc.FBSQLException;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by YR on 10/3/2016.
 */

public class server extends Thread{

    private ServerSocket serverSocket;

    public server() throws IOException {
        serverSocket = new ServerSocket(22);
        //serverSocket.setSoTimeout(10000);
    }

    public static void createTables() throws SQLException, ClassNotFoundException {

        //ArrayList<String> tables = new ArrayList<>();

        Map<String, String> tables = new HashMap<>();

        String table1 = "create table "+ Tables.TABLE_NAME +" ("+ Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+ my.app.yrusanov.theplace.ui.DB.Types.VARCHAR30+", "+Fields.IMAGE+" "+ Types.BLOB_fb+")";
        String table2 = "create table " + Tables.TABLE_NAME_BEERS + " ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.NAME+" "+ Types.VARCHAR150+", "+Fields.ALCOHOL+" "+ Types.VARCHAR30+", "+Fields.WEIGHT+" "+ Types.VARCHAR30+", "+Fields.IMAGE+" "+ Types.BLOB_fb+", "+Fields.DESCRIPTION+" "+ Types.VARCHAR30+", "+Fields.PRICE+" "+ Types.FLOAT+", "+Fields.IDCATEGORY+" "+ Types.INTEGER+", "+Fields.STYLE+" "+Types.VARCHAR30+", "+Fields.COUNTRY+" "+Types.VARCHAR30+")";
        String table3 = "create table " + Tables.TABLE_NAME_ALBUMS  + " ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.NAME+" "+ Types.VARCHAR30+")";
        String table4 = "create table " + Tables.TABLE_NAME_IMAGES + " ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.NAME+" "+ Types.VARCHAR30+", "+Fields.DESCRIPTION+" "+ Types.VARCHAR30+", "+Fields.IMAGE+" "+ Types.BLOB_fb+", "+Fields.IDALBUM+" "+ Types.INTEGER+")";
        String table5 = "create table " + Tables.TABLE_NAME_FOOD_CATEGORY + " ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.NAME+" "+ Types.VARCHAR30+", "+Fields.IMAGE+" "+ Types.BLOB_fb+")";
        String table6 = "create table " + Tables.TABLE_NAME_FOOD + " ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.NAME+" "+ Types.VARCHAR30+", "+Fields.WEIGHT+" "+ Types.VARCHAR30+", "+Fields.PRICE+" "+ Types.FLOAT+", "+Fields.IMAGE+" "+ Types.BLOB_fb+", "+Fields.IDCATEGORY+" "+ Types.INTEGER+")";
        String table7 = "create table " + Tables.TABLE_NAME_MENU_TYPE + " ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.NAME+" "+ Types.VARCHAR30+")";
        String table8 = "create table " + Tables.TABLE_NAME_MENU + " ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY, \""+Fields.DATE+"\" "+Types.DATE+", "+Fields.TYPE+" "+ Types.INTEGER+")";
        String table9 = "create table " + Tables.TABLE_NAME_MENU_CONTENT + " ("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.MENUID+" "+ Types.INTEGER+", "+Fields.FOODID+" "+ Types.INTEGER+")";
        String table10 = "create table " + Tables.TABLE_NAME_LUNCH_WEEK + "("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.WEEKID+" "+ Types.INTEGER+", \""+Fields.DATE+"\" "+ Types.DATE+", "+Fields.MON+" "+ Types.INTEGER+", "+Fields.TUE+" "+ Types.INTEGER+", "+Fields.WED+" "+ Types.INTEGER+", "+Fields.THU+" "+ Types.INTEGER+", "+Fields.FRI+" "+ Types.INTEGER+", "+Fields.SAT+" "+ Types.INTEGER+", "+Fields.SUN+" "+ Types.INTEGER+")";
        String table11 = "create table " + Tables.TABLE_NAME_ITEMTRANSLATION+ "("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.IDITEMTRANSLATION+" "+ Types.INTEGER+", "+Fields.NAME+" "+Types.VARCHAR60+" , "+ Fields.TYPE+" "+Types.VARCHAR30+", "+ Fields.LANGUAGEID+" "+Types.VARCHAR30+", "+Fields.DESCRIPTION+" "+Types.VARCHAR150+")";
        String table12 ="create table " + Tables.TABLE_NAME_NEWS_TYPE+ "("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.VARCHAR30+")";
        String table13 ="create table " + Tables.TABLE_NAME_NEWS+ "("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.VARCHAR30+", "+Fields.DESCRIPTION+" "+Types.VARCHAR500+", "+Fields.IMAGE+" "+Types.BLOB_fb+", "+Fields.IDNEWS+" "+Types.INTEGER+")";

        tables.put(Tables.TABLE_NAME,table1);
        tables.put(Tables.TABLE_NAME_BEERS,table2);
        tables.put(Tables.TABLE_NAME_ALBUMS,table3);
        tables.put(Tables.TABLE_NAME_IMAGES, table4);
        tables.put(Tables.TABLE_NAME_FOOD_CATEGORY,table5);
        tables.put(Tables.TABLE_NAME_FOOD,table6);
        tables.put(Tables.TABLE_NAME_MENU_TYPE,table7);
        tables.put(Tables.TABLE_NAME_MENU,table8);
        tables.put(Tables.TABLE_NAME_MENU_CONTENT,table9);
        tables.put(Tables.TABLE_NAME_LUNCH_WEEK,table10);
        tables.put(Tables.TABLE_NAME_ITEMTRANSLATION, table11);
        tables.put(Tables.TABLE_NAME_NEWS_TYPE, table12);
        tables.put(Tables.TABLE_NAME_NEWS, table13);


        Class.forName("org.firebirdsql.jdbc.FBDriver");
        Connection connection = DriverManager.getConnection("jdbc:firebirdsql:127.0.0.1/3050:C:\\Users\\y.rusanov\\Desktop\\IBExpert\\test.fdb"
                ,"SYSDBA","masterkey");

        Statement stmt = connection.createStatement();
        StringBuilder sqlstmt = new StringBuilder();

        DatabaseMetaData meta = connection.getMetaData();



        for(Object k : tables.keySet()){
            ResultSet res = meta.getTables(null, null, (String) k,
                    new String[] {"TABLE"});
            if(res.next()){
                System.out.println("Tables "+(String) k+" exists");
            }
            else{
                try {

                    connection.createStatement().execute(tables.get(k));
                    System.out.println("Tables "+(String) k+" is created");

                }
                catch (FBSQLException ex){
                    System.out.println("Tables "+(String) k+" is not ok --> "+ex+"");
                }
            }
        }
        /*
        String buildGenerators;
        String buildTriggers;
        for(Object k : tables.keySet()){
            buildGenerators = "CREATE SEQUENCE GEN_"+k+"_ID";
            connection.createStatement().execute(buildGenerators);
            buildTriggers = "create trigger "+k+"_bi for "+k+" active before insert position 0 as begin if (new.id is null) then new.id = gen_id(gen_"+k+"_id,1);end";
            connection.createStatement().execute(buildTriggers);
        }
        */

    }

    public void run() {
        while(true) {
            /*
                            System.out.println("Waiting for client on port " +
                            serverSocket.getLocalPort() + "...");
                            Socket server = serverSocket.accept();
                            SyncServer syncServer = new SyncServer();

                            syncServer.FoodCategorySync(server);
                            syncServer.FoodSync(server);
                            syncServer.MenuTypeSync(server);
                            syncServer.MenuSync(server);
                            syncServer.MenuContentSync(server);
                            syncServer.LunchWeekSync(server);
                            syncServer.BeerCategorySync(server);
                            syncServer.BeerSync(server);
                            syncServer.AlbumSync(server);
                            syncServer.Images(server);
                            syncServer.ItemTranslationSync(server);
                            syncServer.NewsTypeSync(server);
                            syncServer.NewsSync(server);
                            server.close();

                        }catch(SocketTimeoutException s) {
                            System.out.println("Socket timed out!");
                            break;
                        }catch(IOException e) {
                            e.printStackTrace();
                            break;
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        */
        }
    }

    public static void main(String[] args) throws Exception{

        Socket clientSocket = null;
        ServerSocket serverSocket = null;
        server.createTables();
        try {
            Thread t = new server();
            t.start();
        }catch(IOException e) {
            e.printStackTrace();
        }

    }
}
