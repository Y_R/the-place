package my.app.yrusanov.theplace.ui.fragments.news;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.tables.Image;
import my.app.yrusanov.image.ImagesFile;

/**
 * Created by y.rusanov on 2017-3-8.
 */

public class Fragment_news extends android.support.v4.app.Fragment{

    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_NEWS+"");
    View view;
    String path;
    ContentResolver resolver;
    TextView textViewName, textViewDescription;
    ImageView imageView;
    LinearLayout linearLayout;
    public Fragment_news() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_news, container, false);
        Bundle bundle = this.getArguments();
        resolver = view.getContext().getContentResolver();
        textViewName = (TextView)view.findViewById(R.id.textView_news_name);
        imageView = (ImageView)view.findViewById(R.id.imageView_news);
        textViewDescription = (TextView) view.findViewById(R.id.textView_news_description);
        linearLayout = (LinearLayout)view.findViewById(R.id.linearLayout_news);


        String name = this.getActivity().getIntent().getStringExtra("NewsName");//bundle.getString("Name");
        int idNews = this.getActivity().getIntent().getIntExtra("NewsId",0);
        String whereClause = ""+ Fields.IDNEWS+" = "+idNews+"";
        Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
        if(cursor != null){
            cursor.moveToFirst();
            //File imageFile;
            //imageFile = new File(path + "/"+cursor.getString(1)+".jpg");
            textViewName.setText(cursor.getString(1));
            textViewDescription.setText(cursor.getString(2));
            //if(imageFile.exists()){
            //  Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            Bitmap bitmap = ImagesFile.getImageByType(cursor.getString(1), Types.NEWS);
            if(bitmap != null){
                imageView.setImageBitmap(bitmap);
            }
        }
        else{
            linearLayout.setVisibility(View.INVISIBLE);
        }
        //}

        return view;
    }
}
