package my.app.yrusanov.theplace.ui.DB.FireBird;

import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.theplace.ui.DB.Fields;

import org.apache.poi.ss.formula.ptg.MemAreaPtg;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created by y.rusanov on 2016-12-22.
 */

public class DBOperationFB {

    private String table;
    Connection connection;
    public DBOperationFB(String _tableName) {

        try {
            Class.forName("org.firebirdsql.jdbc.FBDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection("jdbc:firebirdsql:127.0.0.1/3050:C:\\Users\\y.rusanov\\Desktop\\IBExpert\\test.fdb"
                    ,"SYSDBA","masterkey");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        table = _tableName;
    }
    private String escapeStr(String _value){
        return "'"+_value+"'";
    }

    public void insert(Map<String, Object> values) throws SQLException {
        Statement stmt = connection.createStatement();
        int rowsUpdated = 0;
        StringBuilder stringBuilder = new StringBuilder(128);
        String comm = "";
        Set keys = values.keySet();
        stringBuilder.append("INSERT INTO ").append(table).append(" (");
        for(Object key : keys){
            stringBuilder.append(comm).append(key.toString().toUpperCase());
            comm = ", ";
        }
        stringBuilder.append(" ) VALUES (");
        comm = " ";
        keys = values.keySet();
        for(Object key : keys){

            if(String.class.isInstance(values.get(key))){
                stringBuilder.append(comm).append(this.escapeStr((String) values.get(key)));
            }
            else if(Integer.class.isInstance(values.get(key))){
                stringBuilder.append(comm).append((Integer) values.get(key));
            }
            else if(Boolean.class.isInstance(values.get(key))){
                stringBuilder.append(comm).append((Boolean) values.get(key));
            }
            else if(Double.class.isInstance(values.get(key))){
                stringBuilder.append(comm).append((Double) values.get(key));
            }
            comm = ", ";
        }
        stringBuilder.append(")");
        stmt.executeUpdate(stringBuilder.toString());
    }

    public ResultSet selectAll() throws SQLException {
        Statement stmt = connection.createStatement();

        ResultSet rs = stmt.executeQuery(
                "SELECT * FROM "+table+"");
        if(!rs.next()){
            rs = null;
        }

        return rs;
    }

    public ResultSet selectAfterId(int lastRecordId) throws SQLException {
        Statement stmt = connection.createStatement();

        ResultSet rs = stmt.executeQuery(
                "SELECT * FROM "+table+" WHERE "+Fields.ID+" > "+lastRecordId+"");

        if(!rs.next())
            rs = null;

        return rs;
    }

    private void update(){

    }

    private boolean checkRecord(){

        return false;
    }

}
