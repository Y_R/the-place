package my.app.yrusanov.theplace.ui.adapters.News;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.tables.NewsType_table;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2017-3-8.
 */

public class RecyclerAdapterNewsType extends RecyclerView.Adapter<RecyclerAdapterNewsType.ViewHolder> {

    private ArrayList<NewsType_table> elements;
    RecyclerAdapterNewsType.OnItemClickListener mItemClickListener;
    NewsType_table newsTypeTable;
    ContentResolver resolver;
    String path;
    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_NEWS_TYPE+"");

    public RecyclerAdapterNewsType(Context _context){

        elements = new ArrayList<>();

        resolver = _context.getContentResolver();
        String[] projection = new String[]{"ID", "NAME"};
        Cursor cursor = resolver.query(CONTENT_URL,projection,null,null,null);
        if(cursor != null){
            cursor.moveToFirst();
            do{

                elements.add(new NewsType_table(cursor.getString(1)));

            }while (cursor.moveToNext());
        }

    }

    @Override
    public RecyclerAdapterNewsType.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerAdapterNewsType.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_news_type,parent,false));
    }

    @Override
    public void onBindViewHolder(RecyclerAdapterNewsType.ViewHolder holder, int position) {
        newsTypeTable = elements.get(position);
        holder.textView_news_name.setText(newsTypeTable.getName());
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        TextView textView_news_name, textView_news_id;
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView_news_name = (TextView)itemView.findViewById(R.id.textView_news_name);
            textView_news_id= (TextView)itemView.findViewById(R.id.textView_news_Idnews);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface  OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final RecyclerAdapterNewsType.OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }

}
