package my.app.yrusanov.theplace.ui.tables;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-9-7.
 */
public class Food {

    private int ID;
    private String Name;
    private String Description;
    private String Weight;
    private float Price;
    private int IDCategory;
    private ArrayList<Food> foodArrayList;
    private Bitmap Image;

    public Food(int ID, String name, String weight, float price, Bitmap _image, int IDCategory) {
        this.ID = ID;
        this.Name = name;
        this.Weight = weight;
        this.Price = price;
        this.IDCategory = IDCategory;
        this.Image = _image;
    }
    public Food(int ID, String name, String weight, float price, int IDCategory) {
        this.ID = ID;
        this.Name = name;
        this.Weight = weight;
        this.Price = price;
        this.IDCategory = IDCategory;
    }
    public Food(String name, String description, String weight, float price, int IDCategory) {
        Name = name;
        Description = description;
        Weight = weight;
        Price = price;
        this.IDCategory = IDCategory;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float price) {
        Price = price;
    }

    public int getIDCategory() {
        return IDCategory;
    }

    public void setIDCategory(int IDCategory) {
        this.IDCategory = IDCategory;
    }

    public ArrayList<Food> getFoodArrayList() {
        return foodArrayList;
    }


    public void setFoodArrayList(ArrayList<?> foodArrayList) {

        ArrayList<Food> localFoodArrayList = new ArrayList<>();
        ArrayList<Object> localArrayList;

        for(int i = 0; i < foodArrayList.size(); i++){
            localArrayList = (ArrayList<Object>) foodArrayList.get(i);

            localFoodArrayList.add(new Food(
                    Integer.parseInt((String) localArrayList.get(0)),
                    (String) localArrayList.get(1),
                    (String) localArrayList.get(2),
                    Float.parseFloat((String) localArrayList.get(3)),
                    (Bitmap) localArrayList.get(5)
                    ,Integer.parseInt((String) localArrayList.get(4))
            ));
        }

        this.foodArrayList = localFoodArrayList;
    }

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }
}
