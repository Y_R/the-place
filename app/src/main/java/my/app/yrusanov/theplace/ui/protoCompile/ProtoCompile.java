package my.app.yrusanov.theplace.ui.protoCompile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by YR on 11/9/2016.
 */

public class ProtoCompile {
    public static void main(String[] args) throws IOException {

        ArrayList<String> listProtoPath = new ArrayList<>();
        String path = System.getProperty("user.dir") + "\\app\\src\\main\\proto";
        System.out.println(path);
        String pathProtoClasses  = System.getProperty("user.dir") + "\\app\\src\\main\\java\\";

        String extensions;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        /*
        for(int i = 0; i < listOfFiles.length; i++){
            int k = listOfFiles[i].getName().lastIndexOf('.');
            extensions = listOfFiles[i].getName().substring(k+1);
            if(extensions.equals("proto")){
                listProtoPath.add(listOfFiles[i].getName());
            }
        }
        */
        listProtoPath.add("PackageMessage.proto");
        if(!listProtoPath.isEmpty()){
            for(int i = 0; i < listProtoPath.size(); i++){
                List cmdAndArgs = Arrays.asList("cmd","/c","proto.exe --java_out=../java/ "+listProtoPath.get(i)+"");
                File dir = new File(path);
                ProcessBuilder pb = new ProcessBuilder(cmdAndArgs);
                pb.directory(dir);
                Process p = pb.start();
                System.out.printf("Proto file: "+listProtoPath.get(i)+" is done/n");
            }
        }
    }
}
