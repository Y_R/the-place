package my.app.yrusanov.theplace.ui.DB;

/**
 * Created by y.rusanov on 2016-9-28.
 */

public class Tables {
    public static final String TABLE_NAME               = "BEERSCATEGORIES";
    public static final String TABLE_NAME_BEERS         = "BEERS";
    public static final String TABLE_NAME_IMAGES        = "IMAGES";
    public static final String TABLE_NAME_ALBUMS        = "ALBUMS";
    public static final String TABLE_NAME_FOOD          = "FOOD";
    public static final String TABLE_NAME_FOOD_CATEGORY = "FOODCATEGORY";
    public static final String TABLE_NAME_MENU_TYPE     = "MENUTYPE";
    public static final String TABLE_NAME_MENU_CONTENT  = "MENUCONTENT";
    public static final String TABLE_NAME_MENU          = "MENU";
    public static final String TABLE_NAME_LUNCH_WEEK    = "LUNCHWEEK";
    public static final String TABLE_NAME_ITEMTRANSLATION = "ITEMTRANSLATION";
    public static final String TABLE_NAME_NEWS_TYPE     = "NEWSTYPE";
    public static final String TABLE_NAME_NEWS          = "NEWS";

}
