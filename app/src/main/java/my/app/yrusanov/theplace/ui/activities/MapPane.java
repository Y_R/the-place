package my.app.yrusanov.theplace.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by y.rusanov on 2017-3-9.
 */

public class MapPane extends AppCompatActivity implements OnMapReadyCallback{

    static final LatLng BeerBoxAddress = new LatLng(42.649400, 23.377810);
    static final LatLng KIEL = new LatLng(53.551, 9.993);
    private GoogleMap map;
    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        TextView feedback = (TextView) findViewById(R.id.textView_address_email);
        feedback.setText(Html.fromHtml("<a href=\"mailto:beerbox@abv.bg\">Email: beerbox@abv.bg</a>"));
        feedback.setMovementMethod(LinkMovementMethod.getInstance());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_beerbox);

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(42.649250, 23.377376))
                .title("Beer Box")
                .icon(icon));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(42.649400, 23.377810), 16));
    }

}