package my.app.yrusanov.theplace.ui.sync.authenticate;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox;
import my.app.yrusanov.theplace.ui.sync.SyncClient;
import my.app.yrusanov.theplace.ui.sync.SyncClient_v2;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by y.rusanov on 2017-2-27.
 */

public class SyncAdapter extends AbstractThreadedSyncAdapter {


    private Context mAppContext;
    private SyncClient_v2 syncClient_v2;
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mAppContext = context;
    }

    private void syncDB_v2_(ContentProviderClient provider) throws IOException, RemoteException, ClassNotFoundException {
        //Socket client = new Socket("192.168.1.103",22);
        Socket client = new Socket("77.77.151.29", 22);
        //Socket client = new Socket("192.168.1.103", 22);
        syncClient_v2 = new SyncClient_v2(this.mAppContext,provider);

        DataOutputStream out = new DataOutputStream(client.getOutputStream());
        out.writeBoolean(true);

        HashMap<Object, Integer> lastRecId =  syncClient_v2.setLastRecId();
        syncClient_v2.sendLastRecId(client,lastRecId);
        //syncClient_v2.syncImages(client);

        try {
            syncClient_v2.sync(client);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        client.close();
    }

    private void deleteDB_(ContentProviderClient provider) throws RemoteException {
        provider.delete(ContentProviderBeerBox.CONTENT_URI_ALBUMS,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_BEER,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_BEERCATEGPRY,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_FOOD,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_FOODCATEGORY,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_IMAGES,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_ITEMTRANSLATION,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_LUNCHWEEK,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_MENU,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_MENUCONTENT,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_MENUTYPE,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_NEWS,null,null);
        provider.delete(ContentProviderBeerBox.CONTENT_URI_NEWS_TYPE,null,null);
        System.out.println("DB reset");
    }

    private void resetDB_(ContentProviderClient provider) throws RemoteException, IOException, SQLException, ClassNotFoundException {
        this.deleteDB_(provider);
        this.syncDB_v2_(provider);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        boolean resetDB = extras.getBoolean("reset");
        boolean deleteDB = extras.getBoolean("delete");
        if(resetDB){
            //SyncClientThread sync = new SyncClientThread(mAppContext, provider, "reset");
            //sync.start();
            try {
                this.resetDB_(provider);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            Log.i("Sync", "DB is reset");
            System.out.println("DB is reset");
        }
        else if(deleteDB){

            //SyncClientThread sync = new SyncClientThread(mAppContext, provider, "delete");
            //sync.start();
            try {
                this.deleteDB_(provider);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            Log.i("Sync", "DB is delete");
            System.out.println("DB is delete");
        }
        else {
            //SyncClientThread sync = new SyncClientThread(mAppContext, provider);
            try {
                this.syncDB_v2_(provider);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            //sync.start();
        }
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    public static class SyncClientThread extends Thread {

        private SyncClient syncClient;
        private SyncClient_v2 syncClient_v2;
        private String operation;
        private ContentProviderClient provider;
        private Context context;
        private ContentProviderClient contentProviderClient;

        public SyncClientThread(Context _context) {
            syncClient = new SyncClient(_context);
        }
        public SyncClientThread(Context _context, ContentProviderClient provider) {
            this.provider = provider;
            this.context = _context;
            //syncClient = new SyncClient(_context,provider);
        }
        public SyncClientThread(Context _context, ContentProviderClient provider, String _operation) {
            this.operation = _operation;
            this.provider = provider;
            this.context = _context;
            //syncClient = new SyncClient(_context,provider);


        }

        private void deleteDB() throws RemoteException {
            provider.delete(ContentProviderBeerBox.CONTENT_URI_ALBUMS,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_BEER,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_BEERCATEGPRY,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_FOOD,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_FOODCATEGORY,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_IMAGES,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_ITEMTRANSLATION,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_LUNCHWEEK,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_MENU,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_MENUCONTENT,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_MENUTYPE,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_NEWS,null,null);
            provider.delete(ContentProviderBeerBox.CONTENT_URI_NEWS_TYPE,null,null);
            System.out.println("DB reset");
        }

        private void resetDB() throws RemoteException, IOException, SQLException, ClassNotFoundException {
            this.deleteDB();
            this.syncDB_v2();
            //this.syncDB();
        }

        private void syncDB() throws SQLException, IOException, RemoteException {
            //Socket client = new Socket("192.168.1.101",22);
            Socket client = new Socket("77.77.151.29", 22);
            //Socket client = new Socket("192.168.1.103", 22);

            System.out.println("Update food category");
            syncClient.FoodCategorySync(client);
            System.out.println("Update food ");
            syncClient.FoodSync(client);
            System.out.println("Update Menu type");
            syncClient.MenuTypeSync(client);
            System.out.println("Update menu");
            syncClient.MenuSync(client);
            System.out.println("Update menu content");
            syncClient.MenuContentSync(client);
            System.out.println("Update Lunch week");
            syncClient.LunchWeekSync(client);
            System.out.println("Update Beer vategory");
            syncClient.BeerCategorySync(client);
            System.out.println("Update beer");
            syncClient.BeerSync(client);
            System.out.println("Update album");
            syncClient.AlbumSync(client);
            System.out.println("Update images");
            syncClient.Images(client);
            System.out.println("Update item trans");
            syncClient.ItemTranslationSync(client);
            System.out.println("Update news type");
            syncClient.NewsTypeSync(client);
            System.out.println("Update news");
            syncClient.NewsSync(client);
            client.close();
        }

        private void syncDB_v2() throws IOException, RemoteException, ClassNotFoundException {
            //Socket client = new Socket("192.168.1.103",22);
            Socket client = new Socket("77.77.151.29", 22);
            //Socket client = new Socket("192.168.1.103", 22);
            syncClient_v2 = new SyncClient_v2(context,provider);

            DataOutputStream out = new DataOutputStream(client.getOutputStream());
            out.writeBoolean(true);

            HashMap<Object, Integer> lastRecId =  syncClient_v2.setLastRecId();
            syncClient_v2.sendLastRecId(client,lastRecId);
            //syncClient_v2.syncImages(client);

            try {
                syncClient_v2.sync(client);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            client.close();
        }
        @Override
        public void run() {
                try {

                    if(java.util.Objects.equals(operation, "reset")){
                        this.resetDB();
                    }
                    else if(java.util.Objects.equals(operation, "delete")){
                       this.deleteDB();
                    }
                    else{
                        this.syncDB_v2();
                        //this.syncDB();
                    }


                    Log.i("Sync", "Syncing Finished");
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (RemoteException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
        }
    }
}
