package my.app.yrusanov.theplace.ui.fragments.menu;


import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_menu_item extends Fragment {

    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_FOOD+"");
    View view;
    String path;
    ContentResolver resolver;
    TextView textViewName;
    ImageView imageView;
    TextView textView_menu_weight,textView_menu_price, textView_menu_id;

    public Fragment_menu_item() {
        // Required empty public constructor
    }
    public String language;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_menu_item, container, false);
        Bundle bundle = this.getArguments();
        resolver = view.getContext().getContentResolver();
        textViewName = (TextView)view.findViewById(R.id.textViewName_menuItem);
        imageView = (ImageView)view.findViewById(R.id.imageProfile_Food);
        textView_menu_price = (TextView)view.findViewById(R.id.textViewPrice_Food);
        textView_menu_weight = (TextView)view.findViewById(R.id.textViewWeight_Food);
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/Food";
        String contentName;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(view.getContext());
        language  = prefs.getString("list_preferences","en");
        String name = bundle.getString("Name");
        int id = bundle.getInt("Id");
        String whereClause = ""+ Fields.ID+" = "+id+"";
        Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
        cursor.moveToFirst();
        File imageFile;
        imageFile = new File(path + "/"+cursor.getString(1)+".jpg");

        contentName = DBOperation.getTranslation(view.getContext(), cursor.getInt(0), Types.FOOD,language,"Name");
        if(contentName.equals("")) {
            contentName = cursor.getString(1);
        }
        textViewName.setText(contentName);

        textView_menu_weight.setText(String.valueOf(cursor.getString(2)));

        textView_menu_price.setText(String.valueOf(cursor.getFloat(3)));
        if(imageFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
        }
        else{
            imageView.setImageBitmap(BitmapFactory.decodeResource(this.getResources(),R.drawable.food));
        }
        return view;

    }

}
