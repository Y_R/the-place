package my.app.yrusanov.theplace.ui.tables;

import java.util.Date;

/**
 * Created by y.rusanov on 2016-9-27.
 */

public class Menu {

    private int ID;
    private Date Date;
    private int Type;

    public Menu(int ID, Date date, int type) {
        this.ID = ID;
        Date = date;
        Type = type;
    }

    public Menu(Date date, int type) {
        Date = date;
        Type = type;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date date) {
        Date = date;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }
}
