package my.app.yrusanov.theplace.ui.fragments.beer;


import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.tables.Beer;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_beer extends Fragment {

    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_BEERS+"");
    View view;
    String path;
    ContentResolver resolver;
    TextView textViewName, textViewCounty, textViewWeight, textViewAlcohol,textViewStyle, textViewPrice;
    ImageView imageView;
    Beer beer;
    public Fragment_beer() {
        // Required empty public constructor
    }
    public String language;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_beer, container, false);
        Bundle bundle = this.getArguments();
        resolver = view.getContext().getContentResolver();
        textViewName = (TextView)view.findViewById(R.id.textViewName);
        textViewAlcohol = (TextView)view.findViewById(R.id.textViewAlcohol);
        textViewCounty = (TextView)view.findViewById(R.id.textViewCountry);
        textViewPrice = (TextView)view.findViewById(R.id.textViewPrice);
        textViewWeight = (TextView)view.findViewById(R.id.textViewWeight);
        textViewStyle = (TextView)view.findViewById(R.id.textViewStyle);
        String contentName;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(view.getContext());
        language  = prefs.getString("list_preferences","en");
        imageView = (ImageView)view.findViewById(R.id.imageProfile_Food);
        //path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/Beer";
        String name = bundle.getString("Name");
        int Id = bundle.getInt("Id");
        String whereClause = ""+ Fields.ID+" = "+Id+"";
        Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/"+ Types.BEER+"";
        cursor.moveToFirst();

        contentName = DBOperation.getTranslation(view.getContext(), cursor.getInt(0), Types.BEER,language,"Name");
        if(contentName.equals("")) {
            contentName = cursor.getString(1);
        }

        File imageFile;
        imageFile = new File(path + "/"+cursor.getString(1)+".jpg");
        if(imageFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            beer = new Beer(cursor.getInt(0), contentName, cursor.getString(3),bitmap, cursor.getFloat(6), cursor.getInt(7), cursor.getString(8), cursor.getString(9), cursor.getString(2));
        }
        else{
            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.beer);
            beer = new Beer(cursor.getInt(0), contentName, cursor.getString(3),bitmap, cursor.getFloat(6), cursor.getInt(7), cursor.getString(8), cursor.getString(9),cursor.getString(2));
        }

        textViewName.setText(beer.getName());
        textViewAlcohol.setText(beer.getAlcohol());
        textViewWeight.setText(beer.getWeight());
        textViewPrice.setText(Float.toString(beer.getPrice()));
        textViewStyle.setText(beer.getStyle());
        textViewCounty.setText(beer.getCountry());
        imageView.setImageBitmap(beer.getImage());

        return view;
    }

}
