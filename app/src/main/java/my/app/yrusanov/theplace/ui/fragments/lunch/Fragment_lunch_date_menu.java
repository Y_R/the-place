package my.app.yrusanov.theplace.ui.fragments.lunch;


import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.adapters.Beers.RecyclerAdapterBeers;
import my.app.yrusanov.theplace.ui.adapters.Lunch.RecyclerAdapterLunchDate;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_lunch_date_menu extends Fragment {


    static final Uri CONTENT_URL_MENUCONTENT = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_MENU_CONTENT+"");
    static final Uri CONTENT_URL_FOOD= Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_FOOD+"");
    View view;
    String path;
    ContentResolver resolver;
    TextView textViewName;
    RecyclerAdapterLunchDate adapter;
    RecyclerView recyclerView;
    LinearLayoutManager mLayoutManager;
    public Fragment_lunch_date_menu() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_lunch_date_menu, container, false);
        Bundle bundle = this.getArguments();

        //getActivity().setTitle(""+bundle.getString("DayLunch")+", "+bundle.getString("DateLunch")+"");
        getActivity().setTitle(""+this.getActivity().getIntent().getStringExtra("DayLunch")+", "+this.getActivity().getIntent().getStringExtra("DateLunch")+"");

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_lunch_date_menu);
        recyclerView.setHasFixedSize(true);

        //setting the layout manager
        mLayoutManager = new LinearLayoutManager(this.getActivity().getBaseContext());
        recyclerView.setLayoutManager(mLayoutManager);

        //setting the items decoration
        //final RecyclerView.ItemDecoration decoration = new ElementDecoraction(this);

        //setting the adapter
        textViewName = (TextView)view.findViewById(R.id.textViewName);
        String IdMenu = this.getActivity().getIntent().getStringExtra("Id");
        adapter = new RecyclerAdapterLunchDate(view.getContext(), Integer.valueOf(IdMenu));

        recyclerView.setAdapter(adapter);

        return view;

    }

}
