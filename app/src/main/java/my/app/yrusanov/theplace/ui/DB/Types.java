package my.app.yrusanov.theplace.ui.DB;

/**
 * Created by YR on 10/7/2016.
 */

public class Types {
    public static final String INTEGER = "INTEGER";
    public static final String TEXT = "TEXT";
    public static final String BLOB = "BLOB";
    public static final String DOUBLE = "DOUBLE";
    public static final String DATE = "DATE";
    public static final String VARCHAR500 = "VARCHAR(500)";
    public static final String VARCHAR60 = "VARCHAR(60)";
    public static final String VARCHAR30 = "VARCHAR(30)";
    public static final String VARCHAR150 = "VARCHAR(150)";

    public static final String FLOAT = "FLOAT";
    public static final String BLOB_fb = "BLOB SUB_TYPE 0 SEGMENT SIZE 16384";
    public static final String BEER = "Beer";
    public static final String FOOD = "Food";
    public static final String IMAGES = "Images";
    public static final String BEERCATEGORIES = "BeerCategories";
    public static final String FOODCATEGORIES = "FoodCategories";
    public static final String NEWS = "News_table";



}
