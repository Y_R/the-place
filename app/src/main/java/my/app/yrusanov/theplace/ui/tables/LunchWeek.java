package my.app.yrusanov.theplace.ui.tables;

import java.util.Date;

/**
 * Created by y.rusanov on 2016-9-27.
 */

public class LunchWeek {

    private int ID;
    private int WEEKID;
    private Date date;
    private int mon;
    private int tue;
    private int wed;
    private int thu;
    private int fri;
    private int sat;
    private int sun;

    public LunchWeek(int ID, int WEEKID, Date date, int mon, int tue, int wed, int thu, int fri, int sat, int sun) {
        this.ID = ID;
        this.WEEKID = WEEKID;
        this.date = date;
        this.mon = mon;
        this.tue = tue;
        this.wed = wed;
        this.thu = thu;
        this.fri = fri;
        this.sat = sat;
        this.sun = sun;
    }

    public LunchWeek(int WEEKID, Date date, int mon, int tue, int wed, int thu, int fri, int sat, int sun) {
        this.WEEKID = WEEKID;
        this.date = date;
        this.mon = mon;
        this.tue = tue;
        this.wed = wed;
        this.thu = thu;
        this.fri = fri;
        this.sat = sat;
        this.sun = sun;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getWEEKID() {
        return WEEKID;
    }

    public void setWEEKID(int WEEKID) {
        this.WEEKID = WEEKID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getMon() {
        return mon;
    }

    public void setMon(int mon) {
        this.mon = mon;
    }

    public int getTue() {
        return tue;
    }

    public void setTue(int tue) {
        this.tue = tue;
    }

    public int getWed() {
        return wed;
    }

    public void setWed(int wed) {
        this.wed = wed;
    }

    public int getThu() {
        return thu;
    }

    public void setThu(int thu) {
        this.thu = thu;
    }

    public int getFri() {
        return fri;
    }

    public void setFri(int fri) {
        this.fri = fri;
    }

    public int getSat() {
        return sat;
    }

    public void setSat(int sat) {
        this.sat = sat;
    }

    public int getSun() {
        return sun;
    }

    public void setSun(int sun) {
        this.sun = sun;
    }
}
