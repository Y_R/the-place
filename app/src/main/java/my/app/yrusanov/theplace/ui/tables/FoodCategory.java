package my.app.yrusanov.theplace.ui.tables;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-9-7.
 */
public class FoodCategory {

    private int ID;
    private String Name;
    private ArrayList<FoodCategory> food_categoryArrayList;
    private Bitmap Image;

    public FoodCategory(int _ID, String _Name, Bitmap _Image){
        this.ID = _ID;
        this.Name = _Name;
        this.Image = _Image;
    }

    public FoodCategory(String _Name, Bitmap _Image){
        this.Name = _Name;
        this.Image = _Image;
    }

    public FoodCategory(int _ID, String _Name){
        this.ID = _ID;
        this.Name = _Name;
    }

    public FoodCategory(String _Name){
        this.Name = _Name;
    }


    public void setID(int _ID){
        this.ID = _ID;
    }

    public void setName(String _Name){
        this.Name = _Name;
    }

    public int getID(){
        return ID;
    }

    public String getName(){
        return Name;
    }

    public ArrayList<FoodCategory> getFood_categoryArrayList() {
        return food_categoryArrayList;
    }

    public void setfood_categoryArrayList(ArrayList<?> food_categoryArrayList) {

        ArrayList<FoodCategory> localFoodCategoryArrayList = new ArrayList<>();
        ArrayList<Object> localArrayList = new ArrayList<>();
        for(int i = 0; i < food_categoryArrayList.size(); i++){
            localArrayList = (ArrayList<Object>) food_categoryArrayList.get(i);

            localFoodCategoryArrayList.add(new FoodCategory(Integer.parseInt((String) localArrayList.get(0)), (String) localArrayList.get(1), (Bitmap) localArrayList.get(2)));
        }
        this.food_categoryArrayList = localFoodCategoryArrayList;
    }


    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }
}
