package my.app.yrusanov.theplace.ui.activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.adapters.Images.RecyclerAdapterAlbums;

public class Gallery extends AppCompatActivity {

    RecyclerView recyclerView;
    //RecyclerAdapter_album adapter;
    RecyclerAdapterAlbums adapter;
    ContentResolver resolver;
    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_ALBUMS+"");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        getSupportActionBar().setTitle(R.string.Albums);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_gallery_album);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new RecyclerAdapterAlbums(this.getBaseContext());

        recyclerView.setAdapter(adapter);


        adapter.SetOnItemClickListener(new RecyclerAdapterAlbums.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView textView_album = (TextView) view.findViewById(R.id.textView_album);
                TextView textView_IDalbum = (TextView) view.findViewById(R.id.textView_IDalbum);
                String albumName;
                Intent intent = new Intent(view.getContext(), fragment_activity.class);
                intent.putExtra("activity", "Gallery_albums");

                resolver = getContentResolver();

                String whereClause = ""+ Fields.NAME+" = '"+textView_album.getText().toString()+"'";

                Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
                cursor.moveToFirst();

                int idAlbum = cursor.getInt(0);//Integer.parseInt(textView_IDalbum.getText().toString());
                albumName = cursor.getString(1);

                intent.putExtra("IdAlbum", idAlbum);
                intent.putExtra("AlbumName", albumName);
                startActivity(intent);
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
