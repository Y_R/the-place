package my.app.yrusanov.theplace.ui.adapters.Images;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.tables.*;
import my.app.yrusanov.image.ImagesFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by y.rusanov on 2016-8-31.
 */
public class RecyclerAdapterAlbums extends RecyclerView.Adapter<RecyclerAdapterAlbums.ViewHolder> {

    ArrayList<GalleryAlbum> elements;

    ArrayList<Image> elementsImage;
    OnItemClickListener mItemClickListener;
    GalleryAlbum album;
    ContentResolver resolver;
    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_ALBUMS+"");
    static final Uri CONTENT_URL_IMAGES = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_IMAGES+"");
    Bitmap bitmap;
    int i;
    Map<Integer, Bitmap> images = new HashMap<>();
    public RecyclerAdapterAlbums(Context _context){

        elements = new ArrayList<>();
        elementsImage = new ArrayList<>();
        resolver = _context.getContentResolver();
        String[] projection = new String[]{"ID", "NAME"};
        Cursor cursor = resolver.query(CONTENT_URL,projection,null,null,null);
        if(cursor != null){
            cursor.moveToFirst();
            do{
                elements.add(new GalleryAlbum(cursor.getInt(0),cursor.getString(1)));
                String whereClause = ""+Fields.IDALBUM+" = "+cursor.getInt(0)+"";
                String orderByFood = ""+Fields.ID+" DESC";
                Cursor cursorImages = resolver.query(CONTENT_URL_IMAGES,null,whereClause,null,orderByFood);
                if (cursorImages != null){
                    cursorImages.moveToFirst();
                    do {
                        i++;
                        bitmap = ImagesFile.getImageByType(cursorImages.getString(1), Types.IMAGES);
                        if (bitmap != null)
                            images.put(i, bitmap);
                        else{
                            bitmap = BitmapFactory.decodeResource(_context.getResources(),R.drawable.image);
                            images.put(i, bitmap);
                        }
                        if(i == 4){
                            break;
                        }
                    }while (cursorImages.moveToNext());
                }
                i=0;
            }while (cursor.moveToNext());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_gallery,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        album = elements.get(position);
        holder.textView_album.setText(album.getName());
        holder.textView_IDAlbum.setText(String.valueOf(album.getID()));

        holder.imageView1.setImageBitmap(images.get(1));
        holder.imageView2.setImageBitmap(images.get(2));
        holder.imageView3.setImageBitmap(images.get(3));
        holder.imageView4.setImageBitmap(images.get(4));

    }

    @Override
    public int getItemCount() {
        return elements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        TextView textView_album;
        TextView textView_IDAlbum;
        ImageView imageView1, imageView2, imageView3, imageView4;
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView_album = (TextView)itemView.findViewById(R.id.textView_album);
            textView_IDAlbum = (TextView)itemView.findViewById(R.id.textView_IDalbum);
            imageView1 = (ImageView)itemView.findViewById(R.id.imageView_album_1);
            imageView2 = (ImageView)itemView.findViewById(R.id.imageView_album_2);
            imageView3 = (ImageView)itemView.findViewById(R.id.imageView_album_3);
            imageView4 = (ImageView)itemView.findViewById(R.id.imageView_album_4);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface  OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }
}
