package my.app.yrusanov.theplace.ui.sync;

/**
 * Created by y.rusanov on 2017-6-24.
 */

public class ImageStruct {

    String name;
    String type;

    public ImageStruct(String _name, String _type){
        this.name = _name;
        this.type = _type;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

}
