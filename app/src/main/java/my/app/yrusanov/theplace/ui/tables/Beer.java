package my.app.yrusanov.theplace.ui.tables;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-9-7.
 */
public class Beer {

    private int ID;
    private String Name;
    private String Alcohol;
    private Bitmap Image;
    private String Description;
    int IDCategory;
    private String Weight;
    private float Price;

    public String getStyle() {
        return Style;
    }

    public void setStyle(String style) {
        Style = style;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    private String Style;
    private String Country;

    private ArrayList<Beer> beerArrayList;


    public Beer(int _ID, String _Name, String _Alcohol, Bitmap _Image, String _Description, String _Weight, float _price, int _IDCategory){
        this.ID = _ID;
        this.Name = _Name;
        this.Alcohol = _Alcohol;
        this.Image = _Image;
        this.Description = _Description;
        this.IDCategory = _IDCategory;
        this.Weight = _Weight;
        this.Price = _price;
    }

    public Beer(int _ID, String _Name, String _Alcohol, String _Description,String _Weight, float _price, int _IDCategory){
        this.ID = _ID;
        this.Name = _Name;
        this.Alcohol = _Alcohol;
        this.Description = _Description;
        this.IDCategory = _IDCategory;
        this.Weight = _Weight;
        this.Price = _price;
    }
    public Beer(int _ID, String _Name, String _Alcohol, String _Description, int _IDCategory){
        this.ID = _ID;
        this.Name = _Name;
        this.Alcohol = _Alcohol;
        this.Description = _Description;
        this.IDCategory = _IDCategory;
    }

    public Beer(String _Name, String _Alcohol, String _Description,String _Weight, float _price, int _IDCategory){
        this.Name = _Name;
        this.Alcohol = _Alcohol;
        this.Description = _Description;
        this.IDCategory = _IDCategory;
        this.Weight = _Weight;
        this.Price = _price;
    }


    public Beer(Beer _beer){
        this.ID = _beer.getID();
        this.Name = _beer.getName();
        this.Alcohol = _beer.getAlcohol();
        this.Description = _beer.getDescription();
        this.IDCategory = _beer.getIDCategory();
        this.Weight = _beer.getWeight();
        this.Price = _beer.getPrice();
    }

    public Beer(int _ID, String _Name, String _Weight, Bitmap _image, float _price, int _IDCategory){
        this.ID = _ID;
        this.Name = _Name;
        this.IDCategory = _IDCategory;
        this.Weight = _Weight;
        this.Price = _price;
        this.Image = _image;
    }

    public Beer(int _ID, String _Name, String _Weight, Bitmap _image, float _price, int _IDCategory, String _style, String _country, String _alcohol){
        this.ID = _ID;
        this.Name = _Name;
        this.IDCategory = _IDCategory;
        this.Weight = _Weight;
        this.Price = _price;
        this.Image = _image;
        this.Style = _style;
        this.Country = _country;
        this.Alcohol = _alcohol;
    }

    public Beer(ArrayList<Object> beerArrayList){
        this.setBeerArrayList(beerArrayList);
    }

    public void setID(int _ID){
        this.ID = _ID;
    }

    public void setName(String _Name){
        this.Name = _Name;
    }

    public void setAlcohol(String _Alcohol){
        this.Alcohol = _Alcohol;
    }

    public void setImage(Bitmap _Image){
        this.Image = _Image;
    }

    public void setDescription(String _Description){
        this.Description = _Description;
    }

    public void setIDCategory(int _IDCategory){
        this.IDCategory = _IDCategory;
    }

    public int getID(){
        return ID;
    }

    public String getName(){
        return Name;
    }

    public String getAlcohol(){
        return Alcohol;
    }

    public Bitmap getImage(){
        return Image;
    }

    public String getDescription(){
        return Description;
    }

    public int getIDCategory(){
        return IDCategory;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float price) {
        Price = price;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }


    public ArrayList<Beer> getBeerArrayList() {
        return beerArrayList;
    }

    public void setBeerArrayList(ArrayList<?> beerArrayList) {

        ArrayList<Beer> localBeerArrayList = new ArrayList<>();
        ArrayList<Object> localArrayList;

        for(int i = 0; i < beerArrayList.size(); i++){
            localArrayList = (ArrayList<Object>) beerArrayList.get(i);
            if(localArrayList.size() == 0)
                continue;

            localBeerArrayList.add(new Beer(
                    Integer.parseInt((String) localArrayList.get(0)),
                    (String) localArrayList.get(1),
                    (String) localArrayList.get(2),
                    (Bitmap) localArrayList.get(3),
                    Float.parseFloat((String) localArrayList.get(4)),
                    Integer.parseInt((String) localArrayList.get(5))
                    ));
        }

        this.beerArrayList = localBeerArrayList;
    }
}
