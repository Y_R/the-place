package my.app.yrusanov.theplace.ui.DB;

/**
 * Created by YR on 10/7/2016.
 */

public class Fields {
    public static final String ID = "ID";
    public static final String NAME = "NAME";
    public static final String ALCOHOL = "ALCOHOL";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String IMAGE = "IMAGE";
    public static final String IDCATEGORY = "IDCATEGORY";
    public static final String IDALBUM = "IDALBUM";
    public static final String WEIGHT = "WEIGHT";
    public static final String IDITEMTRANSLATION = "IDITEMTRANSLATION";
    public static final String PRICE = "PRICE";
    public static final String DATE = "DATE";
    public static final String MENUID = "MENUID";
    public static final String FOODID = "FOODID";
    public static final String WEEKID = "WEEKID";
    public static final String TYPE = "TYPE";
    public static final String MON = "MON";
    public static final String TUE = "TUE";
    public static final String WED = "WED";
    public static final String THU = "THU";
    public static final String FRI = "FRI";
    public static final String SAT = "SAT";
    public static final String SUN = "SUN";
    public static final String LANGUAGEID = "LANGUAGEID";
    public static final String STYLE = "STYLE";
    public static final String COUNTRY = "COUNTRY";
    public static final String IDNEWS = "IDNEWS";
}
