package my.app.yrusanov.theplace.ui.DB;

/**
 * Created by YR on 10/7/2016.
 */

public class FieldType {


    public String ID(){
        return ""+Fields.ID+" "+Types.INTEGER+"";
    }

    public String NAME(){
        return ""+Fields.NAME+" "+Types.TEXT+"";
    }


    public String ALCOHOL(){
        return ""+Fields.ALCOHOL+" "+Types.TEXT+"";
    }

    public String DESCRIPTION(){
        return ""+Fields.DESCRIPTION+" "+Types.TEXT+"";
    }

    public String IMAGE(){
        return ""+Fields.IMAGE+" "+Types.BLOB+"";
    }

    public String IDCATEGORY(){
        return ""+Fields.IDCATEGORY+" "+Types.INTEGER+"";
    }

    public String IDALBUM(){
        return ""+Fields.IDALBUM+" "+Types.INTEGER+"";
    }

    public String WEIGHT(){
        return ""+Fields.WEIGHT+" "+Types.TEXT+"";
    }

    public String PRICE(){
        return ""+Fields.PRICE+" "+Types.FLOAT+"";
    }

    public String DATE(){
        return ""+Fields.DATE+" "+Types.DATE+"";
    }

    public String MENUID(){
        return ""+Fields.MENUID+" "+Types.INTEGER+"";
    }

    public String FOODID(){
        return ""+Fields.FOODID+" "+Types.INTEGER+"";
    }

    public String WEEKID(){
        return ""+Fields.WEEKID+" "+Types.INTEGER+"";
    }

    public String MON(){
        return ""+Fields.MON+" "+Types.INTEGER+"";
    }

    public String TUE(){
        return ""+Fields.TUE+" "+Types.INTEGER+"";
    }

    public String WED(){
        return ""+Fields.WED+" "+Types.INTEGER+"";
    }

    public String THU(){
        return ""+Fields.THU+" "+Types.INTEGER+"";
    }

    public String FRI(){
        return ""+Fields.FRI+" "+Types.INTEGER+"";
    }

    public String SAT(){
        return ""+Fields.SAT+" "+Types.INTEGER+"";
    }

    public String SUN(){
        return ""+Fields.SUN+" "+Types.INTEGER+"";
    }
    public String TYPE(){
        return ""+Fields.TYPE+" "+Types.INTEGER+"";
    }

}
