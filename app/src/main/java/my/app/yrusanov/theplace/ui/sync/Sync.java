package my.app.yrusanov.theplace.ui.sync;

import android.os.RemoteException;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;

/**
 * Created by y.rusanov on 2017-1-4.
 */

public interface Sync {
    void BeerCategorySync(Socket client) throws IOException, SQLException, RemoteException;
    void BeerSync(Socket client) throws IOException, SQLException, RemoteException;
    void MenuTypeSync(Socket client) throws IOException, SQLException, RemoteException;
    void MenuSync(Socket client) throws IOException, SQLException, RemoteException;
    void MenuContentSync(Socket client) throws IOException, SQLException, RemoteException;
    void FoodCategorySync(Socket client) throws IOException, SQLException, RemoteException;
    void FoodSync(Socket client) throws IOException, SQLException, RemoteException;
    void LunchWeekSync(Socket client) throws IOException, SQLException, RemoteException;
    void AlbumSync(Socket client) throws IOException, SQLException, RemoteException;
    void Images(Socket client) throws IOException, SQLException, RemoteException;
    void ItemTranslationSync(Socket client) throws IOException, SQLException, RemoteException;
    void NewsTypeSync(Socket client) throws IOException, SQLException, RemoteException;
    void NewsSync(Socket client) throws IOException, SQLException, RemoteException;

}
