package my.app.yrusanov.theplace.ui.tables;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-9-7.
 */
public class GalleryAlbum {

    private int ID;
    private String Name;
    private ArrayList<GalleryAlbum> galleryAlbumArrayList;

    public GalleryAlbum(int _ID, String _Name){
        this.ID = _ID;
        this.Name = _Name;
    }

    public GalleryAlbum(String _Name){
        this.Name = _Name;
    }

    public void setID(int _ID){
        this.ID = _ID;
    }

    public void setName(String _Name){
        this.Name = _Name;
    }

    public int getID(){
        return ID;
    }

    public String getName(){
        return Name;
    }

    public ArrayList<GalleryAlbum> getGalleryAlbumArrayList() {
        return galleryAlbumArrayList;
    }

    public void setGalleryAlbumArrayList(ArrayList<?> galleryAlbumArrayList) {

        ArrayList<GalleryAlbum> localGalleryAlbumArrayList = new ArrayList<>();
        ArrayList<Object> localArrayList = new ArrayList<>();
        for(int i = 0; i < galleryAlbumArrayList.size(); i++){
            localArrayList = (ArrayList<Object>) galleryAlbumArrayList.get(i);

            localGalleryAlbumArrayList.add(new GalleryAlbum(Integer.parseInt((String) localArrayList.get(0)), (String) localArrayList.get(1)));
        }
        this.galleryAlbumArrayList = localGalleryAlbumArrayList;
    }
}
