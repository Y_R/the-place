package my.app.yrusanov.theplace.ui.activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.adapters.Menu.RecyclerAdapterMenuCategories;

public class Menu extends AppCompatActivity {

    RecyclerView recyclerView;
    //RecyclerAdapter_menu_categories adapter;
    RecyclerAdapterMenuCategories adapter;
    ContentResolver resolver;

    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_FOOD_CATEGORY+"");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getSupportActionBar().setTitle(R.string.title_activity_menu);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_menu_categories);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new RecyclerAdapterMenuCategories(this.getBaseContext());

        recyclerView.setAdapter(adapter);


        adapter.SetOnItemClickListener(new RecyclerAdapterMenuCategories.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView textView_menu_categories = (TextView) view.findViewById(R.id.textView_menu_categories);
                TextView textView_menu_IDCategories = (TextView) view.findViewById(R.id.textView_menu_IDcategories);

                Intent intent = new Intent(view.getContext(), fragment_activity.class);
                intent.putExtra("activity", "Menu_categories");
                resolver = getContentResolver();

                String whereClause = ""+ Fields.ID+" = "+textView_menu_IDCategories.getText().toString()+"";

                Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
                cursor.moveToFirst();
                int idCategory = cursor.getInt(0); //Integer.parseInt(textView_beer_IdCategory.getText().toString());
                intent.putExtra("IdCategory", idCategory);
                startActivity(intent);
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
