package my.app.yrusanov.theplace.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.activities.MainActivity;

/**
 * Created by y.rusanov on 2017-2-20.
 */

public class SplashScreen extends AppCompatActivity {

    Animation animFadeIn, animFadeOut;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        final ImageView imageView = (ImageView)findViewById(R.id.imageView_splash);

        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fadein);
        animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fadeout);
        imageView.startAnimation(animFadeIn);
        animFadeIn.setAnimationListener(new Animation.AnimationListener(){
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.startAnimation(animFadeOut);
                Intent i = new Intent(SplashScreen.this,MainActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
}
