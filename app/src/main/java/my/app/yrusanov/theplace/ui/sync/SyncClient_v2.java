package my.app.yrusanov.theplace.ui.sync;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;

import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox;
import my.app.yrusanov.theplace.ui.protoCompile.PackageProtos;
import my.app.yrusanov.image.ImagesFile;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by y.rusanov on 2017-6-19.
 */

public class SyncClient_v2 implements Sync_v2 {

    private Context context;
    private ContentProviderClient provider;
    HashMap<String, Uri> mapTableUri = new HashMap<>();
    PackageProtos.PackageMessage all;

    public SyncClient_v2(Context _context, ContentProviderClient contentProviderClient){
        context = _context;
        this.provider = contentProviderClient;

        mapTableUri.put(Tables.TABLE_NAME, ContentProviderBeerBox.CONTENT_URI_BEERCATEGPRY);
        mapTableUri.put(Tables.TABLE_NAME_BEERS,ContentProviderBeerBox.CONTENT_URI_BEER);
        mapTableUri.put(Tables.TABLE_NAME_FOOD, ContentProviderBeerBox.CONTENT_URI_FOOD);
        mapTableUri.put(Tables.TABLE_NAME_FOOD_CATEGORY,ContentProviderBeerBox.CONTENT_URI_FOODCATEGORY);
        mapTableUri.put(Tables.TABLE_NAME_ALBUMS,ContentProviderBeerBox.CONTENT_URI_ALBUMS);
        mapTableUri.put(Tables.TABLE_NAME_IMAGES,ContentProviderBeerBox.CONTENT_URI_IMAGES);
        mapTableUri.put(Tables.TABLE_NAME_ITEMTRANSLATION, ContentProviderBeerBox.CONTENT_URI_ITEMTRANSLATION);
        mapTableUri.put(Tables.TABLE_NAME_LUNCH_WEEK,ContentProviderBeerBox.CONTENT_URI_LUNCHWEEK);
        mapTableUri.put(Tables.TABLE_NAME_MENU,ContentProviderBeerBox.CONTENT_URI_MENU);
        mapTableUri.put(Tables.TABLE_NAME_MENU_CONTENT,ContentProviderBeerBox.CONTENT_URI_MENUCONTENT);
        mapTableUri.put(Tables.TABLE_NAME_MENU_TYPE, ContentProviderBeerBox.CONTENT_URI_MENUTYPE);
        mapTableUri.put(Tables.TABLE_NAME_NEWS, ContentProviderBeerBox.CONTENT_URI_NEWS);
        mapTableUri.put(Tables.TABLE_NAME_NEWS_TYPE, ContentProviderBeerBox.CONTENT_URI_NEWS_TYPE);
    }

    public SyncClient_v2(Context _context){
        context = _context;
        mapTableUri.put(Tables.TABLE_NAME, ContentProviderBeerBox.CONTENT_URI_BEERCATEGPRY);
        mapTableUri.put(Tables.TABLE_NAME_BEERS,ContentProviderBeerBox.CONTENT_URI_BEER);
        mapTableUri.put(Tables.TABLE_NAME_FOOD, ContentProviderBeerBox.CONTENT_URI_FOOD);
        mapTableUri.put(Tables.TABLE_NAME_FOOD_CATEGORY,ContentProviderBeerBox.CONTENT_URI_FOODCATEGORY);
        mapTableUri.put(Tables.TABLE_NAME_ALBUMS,ContentProviderBeerBox.CONTENT_URI_ALBUMS);
        mapTableUri.put(Tables.TABLE_NAME_IMAGES,ContentProviderBeerBox.CONTENT_URI_IMAGES);
        mapTableUri.put(Tables.TABLE_NAME_ITEMTRANSLATION, ContentProviderBeerBox.CONTENT_URI_ITEMTRANSLATION);
        mapTableUri.put(Tables.TABLE_NAME_LUNCH_WEEK,ContentProviderBeerBox.CONTENT_URI_LUNCHWEEK);
        mapTableUri.put(Tables.TABLE_NAME_MENU,ContentProviderBeerBox.CONTENT_URI_MENU);
        mapTableUri.put(Tables.TABLE_NAME_MENU_CONTENT,ContentProviderBeerBox.CONTENT_URI_MENUCONTENT);
        mapTableUri.put(Tables.TABLE_NAME_MENU_TYPE, ContentProviderBeerBox.CONTENT_URI_MENUTYPE);
        mapTableUri.put(Tables.TABLE_NAME_NEWS, ContentProviderBeerBox.CONTENT_URI_NEWS);
        mapTableUri.put(Tables.TABLE_NAME_NEWS_TYPE, ContentProviderBeerBox.CONTENT_URI_NEWS_TYPE);
    }

    public SyncClient_v2(ContentProviderClient provider) {
        this.provider = provider;
        mapTableUri.put(Tables.TABLE_NAME, ContentProviderBeerBox.CONTENT_URI_BEERCATEGPRY);
        mapTableUri.put(Tables.TABLE_NAME_BEERS,ContentProviderBeerBox.CONTENT_URI_BEER);
        mapTableUri.put(Tables.TABLE_NAME_FOOD, ContentProviderBeerBox.CONTENT_URI_FOOD);
        mapTableUri.put(Tables.TABLE_NAME_FOOD_CATEGORY,ContentProviderBeerBox.CONTENT_URI_FOODCATEGORY);
        mapTableUri.put(Tables.TABLE_NAME_ALBUMS,ContentProviderBeerBox.CONTENT_URI_ALBUMS);
        mapTableUri.put(Tables.TABLE_NAME_IMAGES,ContentProviderBeerBox.CONTENT_URI_IMAGES);
        mapTableUri.put(Tables.TABLE_NAME_ITEMTRANSLATION, ContentProviderBeerBox.CONTENT_URI_ITEMTRANSLATION);
        mapTableUri.put(Tables.TABLE_NAME_LUNCH_WEEK,ContentProviderBeerBox.CONTENT_URI_LUNCHWEEK);
        mapTableUri.put(Tables.TABLE_NAME_MENU,ContentProviderBeerBox.CONTENT_URI_MENU);
        mapTableUri.put(Tables.TABLE_NAME_MENU_CONTENT,ContentProviderBeerBox.CONTENT_URI_MENUCONTENT);
        mapTableUri.put(Tables.TABLE_NAME_MENU_TYPE, ContentProviderBeerBox.CONTENT_URI_MENUTYPE);
        mapTableUri.put(Tables.TABLE_NAME_NEWS, ContentProviderBeerBox.CONTENT_URI_NEWS);
        mapTableUri.put(Tables.TABLE_NAME_NEWS_TYPE, ContentProviderBeerBox.CONTENT_URI_NEWS_TYPE);
    }

    public void syncImages(Socket client) throws IOException, ClassNotFoundException {
        HashMap<String, byte[]> mapImage = new HashMap<>();
        Object objectImage;
        System.out.println("Predi");
        ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream());
        System.out.println("Sled");
        objectImage = objectInputStream.readObject();
        //mapImage= (HashMap<String, byte[]>) objectInputStream.readObject();
        String split = "";
        System.out.println(objectImage.toString());
        Set<String> keys = mapImage.keySet();
        for (Object key : keys) {
            split = (String)key;

                String[] animalsArray = split.split("-_-");
                switch (animalsArray[0]){
                case Tables.TABLE_NAME:
                    System.out.println( Tables.TABLE_NAME +"-> " + animalsArray[1] +": " + mapImage.get(key).length);
                    break;
                case Tables.TABLE_NAME_BEERS:
                    System.out.println( Tables.TABLE_NAME_BEERS+"-> " + animalsArray[1] +": " + mapImage.get(key).length);
                    break;
                default:
                    System.out.println("Not image for sync");
            }
        }
    }

    @Override
    public HashMap<Object, Integer> setLastRecId() throws RemoteException {

        HashMap<Object, Integer> lastRecIdTable = new HashMap<>();
        Set<String> keys = mapTableUri.keySet();
        for (Object key : keys) {
            Uri uri = mapTableUri.get(key);
            Cursor cursor = provider.query(uri,null,null,null,null);
            if(cursor != null){
                cursor.moveToLast();
                lastRecIdTable.put(key, cursor.getInt(0));
            }
            else {
                lastRecIdTable.put(key, 0);
            }
        }
        return lastRecIdTable;
    }

    @Override
    public void sendLastRecId(Socket client,HashMap<Object, Integer> mapLastRecId) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
        out.writeObject(mapLastRecId);
    }

    @Override
    public String getImage(ByteString message, String _name, String _type) {
            ImagesFile imagesFile = null;
            byte[] dyteImage = new byte[message.size()];
            message.copyTo(dyteImage, 0);

            if (dyteImage.length > 0) {
                HashMap<String, byte[]> mapValue = new HashMap<>();
                mapValue.put(_name, dyteImage);
                imagesFile = new ImagesFile(context, mapValue, _type);
            }
            if(imagesFile != null){
                return imagesFile.getFilePath();
            }
            else
                return "";
    }

    public void sync(Socket client) throws IOException, SQLException, RemoteException {
        all = PackageProtos.PackageMessage.parseDelimitedFrom(client.getInputStream());
        Set<String> keys = mapTableUri.keySet();
        for (Object key : keys) {
            switch ((String) key) {
                case Tables.TABLE_NAME:
                    this.BeerCategorySync();
                    break;
                case Tables.TABLE_NAME_BEERS:
                    this.BeerSync();
                    break;
                case Tables.TABLE_NAME_ALBUMS:
                    this.AlbumSync();
                    break;
                case Tables.TABLE_NAME_IMAGES:
                    this.Images();
                    break;
                case Tables.TABLE_NAME_NEWS_TYPE:
                    this.NewsTypeSync();
                    break;
                case Tables.TABLE_NAME_NEWS:
                    this.NewsSync();
                    break;
                case Tables.TABLE_NAME_MENU_CONTENT:
                    this.MenuContentSync();
                    break;
                case Tables.TABLE_NAME_MENU:
                    this.MenuSync();
                    break;
                case Tables.TABLE_NAME_MENU_TYPE:
                    this.MenuTypeSync();
                    break;
                case Tables.TABLE_NAME_FOOD_CATEGORY:
                    this.FoodCategorySync();
                    break;
                case Tables.TABLE_NAME_FOOD:
                    this.FoodSync();
                    break;
                case Tables.TABLE_NAME_LUNCH_WEEK:
                    this.LunchWeekSync();
                    break;
                default:
                    System.out.println("Not table for sync");
            }
        }
    }

    @Override
    public void BeerCategorySync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_BEERCATEGPRY;

        ContentValues values = new ContentValues();

        for(PackageProtos.BeerCategory protoRow : all.getBeerCategoryList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.NAME, protoRow.getName());

                String imagePath = this.getImage(protoRow.getImage(), protoRow.getName(), Types.BEERCATEGORIES);
                if (!imagePath.equals("")) {
                    values.put(Fields.IMAGE, imagePath);
                }
                provider.insert(CONTENT_URL, values);
                System.out.println(CONTENT_URL + ": " + protoRow.getId());
            }
        }
    }

    @Override
    public void BeerSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_BEER;

        ContentValues values = new ContentValues();

        for(PackageProtos.Beer protoRow : all.getBeerList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.NAME, protoRow.getName());
                values.put(Fields.WEIGHT, protoRow.getWeight());
                values.put(Fields.ALCOHOL, protoRow.getAlcohol());
                values.put(Fields.PRICE, protoRow.getPrice());
                values.put(Fields.IDCATEGORY, protoRow.getIdCategory());
                values.put(Fields.COUNTRY, protoRow.getCountry());
                values.put(Fields.STYLE, protoRow.getStyle());

                String imagePath = this.getImage(protoRow.getImage(), protoRow.getName(), Types.BEER);
                if(!imagePath.equals("")){
                    values.put(Fields.IMAGE, imagePath);
                }
                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void MenuTypeSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_MENUTYPE;

        ContentValues values = new ContentValues();

        for(PackageProtos.MenuType protoRow : all.getMenuTypeList()) {
            if (protoRow.getId() != 0) {
                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.NAME, protoRow.getName());
                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void MenuSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_MENU;
        String DATE_FORMAT_NOW = "dd-MM-yyyy";
        Date date;
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        String stringDate;
        ContentValues values = new ContentValues();

        for(PackageProtos.Menu protoRow : all.getMenuList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());

                stringDate = protoRow.getDate();
                try {
                    date = sdf.parse(stringDate);
                    values.put(Fields.DATE, sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                values.put(Fields.TYPE, protoRow.getType());

                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void MenuContentSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_MENUCONTENT;

        ContentValues values = new ContentValues();

        for(PackageProtos.MenuContent protoRow : all.getMenuContentList()) {
            if (protoRow.getId() != 0) {
                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.MENUID, protoRow.getMenuId());
                values.put(Fields.FOODID, protoRow.getFoodId());
                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void FoodCategorySync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_FOODCATEGORY;

        ContentValues values = new ContentValues();

        for(PackageProtos.FoodCategory protoRow : all.getFoodCategoryList()) {
            if (protoRow.getId() != 0) {
                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.NAME, protoRow.getName());

                String imagePath = this.getImage(protoRow.getImage(), protoRow.getName(), Types.FOODCATEGORIES);
                if(!imagePath.equals("")){
                    values.put(Fields.IMAGE, imagePath);
                }
                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void FoodSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_FOOD;

        ContentValues values = new ContentValues();

        for(PackageProtos.Food protoRow : all.getFoodList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.NAME, protoRow.getName());
                values.put(Fields.WEIGHT, protoRow.getWeight());
                values.put(Fields.PRICE, protoRow.getPrice());
                values.put(Fields.IDCATEGORY, protoRow.getIdCategory());

                String imagePath = this.getImage(protoRow.getImage(), protoRow.getName(), Types.FOOD);
                if(!imagePath.equals("")){
                    values.put(Fields.IMAGE, imagePath);
                }

                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void LunchWeekSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_LUNCHWEEK;
        String DATE_FORMAT_NOW = "dd-MM-yyyy";
        Date date;
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        String stringDate;
        ContentValues values = new ContentValues();

        for(PackageProtos.LunchWeek protoRow : all.getLunchWeekList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.WEEKID, protoRow.getWeekId());
                values.put(Fields.DATE, protoRow.getDate());
                values.put(Fields.MON, protoRow.getMon());
                values.put(Fields.TUE, protoRow.getTue());
                values.put(Fields.WED, protoRow.getWed());
                values.put(Fields.THU, protoRow.getThu());
                values.put(Fields.FRI, protoRow.getFri());
                values.put(Fields.SAT, protoRow.getSat());
                values.put(Fields.SUN, protoRow.getSun());

                stringDate = protoRow.getDate();
                try {
                    date = sdf.parse(stringDate);
                    values.put(Fields.DATE, sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void AlbumSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_ALBUMS;

        ContentValues values = new ContentValues();

        for(PackageProtos.Albums protoRow : all.getAlbumsList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.NAME, protoRow.getName());

                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void Images() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_IMAGES;

        ContentValues values = new ContentValues();

        for(PackageProtos.Images protoRow : all.getImagesList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.NAME, protoRow.getName());
                values.put(Fields.DESCRIPTION, protoRow.getDescription());
                values.put(Fields.IDALBUM, protoRow.getIdAlbum());

                String imagePath = this.getImage(protoRow.getImage(),protoRow.getName(), Types.IMAGES);
                if(!imagePath.equals("")){
                    values.put(Fields.IMAGE, imagePath);
                }

                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void ItemTranslationSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_ITEMTRANSLATION;

        ContentValues values = new ContentValues();

        for(PackageProtos.ItemTranslation protoRow : all.getItemTranslationList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.IDITEMTRANSLATION, protoRow.getIditemtranslation());
                values.put(Fields.TYPE, protoRow.getType());
                values.put(Fields.LANGUAGEID, protoRow.getLanguageId());
                values.put(Fields.NAME, protoRow.getName());

                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void NewsTypeSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_NEWS_TYPE;

        ContentValues values = new ContentValues();

        for(PackageProtos.NewsType protoRow : all.getNewsTypeList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.NAME, protoRow.getName());

                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void NewsSync() throws IOException, SQLException, RemoteException {
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_NEWS;

        ContentValues values = new ContentValues();

        for(PackageProtos.News protoRow : all.getNewsList()) {
            if (protoRow.getId() != 0) {

                values.put(Fields.ID, protoRow.getId());
                values.put(Fields.NAME, protoRow.getName());
                values.put(Fields.DESCRIPTION, protoRow.getDescription());
                values.put(Fields.IDNEWS, protoRow.getIdNews());

                String imagePath = this.getImage(protoRow.getImage(), protoRow.getName(), Types.NEWS);
                if(!imagePath.equals("")){
                    values.put(Fields.IMAGE, imagePath);
                }

                provider.insert(CONTENT_URL, values);
            }
        }
    }

    @Override
    public void sendProtoMessage() throws RemoteException {

    }

    @Override
    public HashMap<Object, Integer> getLastRecId() throws IOException, ClassNotFoundException {
        return null;
    }
}
