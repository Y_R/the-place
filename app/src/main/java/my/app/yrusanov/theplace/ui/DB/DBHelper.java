package my.app.yrusanov.theplace.ui.DB;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by y.rusanov on 2016-9-26.
 */

public class DBHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "BeerBox.db";
    private FieldType fieldType;
    private String table;
    SQLiteDatabase db;
    Context context;
    public DBHelper(Context context, String _table) {
        super(context, DATABASE_NAME, null, 1);
        this.table = _table;
        this.context = context;
        open();
    }

    public DBHelper(Context context){
        super(context, DATABASE_NAME,null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+ Tables.TABLE_NAME +" ("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.TEXT+", "+Fields.IMAGE+" "+ Types.TEXT+")");
        db.execSQL("create table " + Tables.TABLE_NAME_BEERS + " ("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.TEXT+", "+Fields.ALCOHOL+" "+Types.TEXT+", "+Fields.WEIGHT+" "+ Types.TEXT+", "+Fields.IMAGE+" "+ Types.TEXT+", "+Fields.DESCRIPTION+" "+Types.TEXT+", "+Fields.PRICE+" "+Types.FLOAT+", "+Fields.IDCATEGORY+" "+ Types.INTEGER+", "+Fields.STYLE+" "+Types.TEXT+", "+Fields.COUNTRY+" "+Types.TEXT+")");
        db.execSQL("create table " + Tables.TABLE_NAME_ALBUMS  + " ("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.TEXT+")");
        db.execSQL("create table " + Tables.TABLE_NAME_IMAGES + " ("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.TEXT+", "+Fields.DESCRIPTION+" "+Types.TEXT+", "+Fields.IMAGE+" "+ Types.TEXT+", "+Fields.IDALBUM+" "+Types.INTEGER+")");
        db.execSQL("create table " + Tables.TABLE_NAME_FOOD_CATEGORY + " ("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.TEXT+", "+Fields.IMAGE+" "+ Types.TEXT+")");
        db.execSQL("create table " + Tables.TABLE_NAME_FOOD + " ("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.TEXT+", "+Fields.WEIGHT+" "+Types.TEXT+", "+Fields.PRICE+" "+Types.FLOAT+", "+Fields.IMAGE+" "+ Types.TEXT+", "+Fields.IDCATEGORY+" "+ Types.INTEGER+")");
        db.execSQL("create table " + Tables.TABLE_NAME_MENU_TYPE + " ("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.TEXT+")");
        db.execSQL("create table " + Tables.TABLE_NAME_MENU + " ("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.DATE+" "+Types.DATE+", "+Fields.TYPE+" "+Types.INTEGER+")");
        db.execSQL("create table " + Tables.TABLE_NAME_MENU_CONTENT + " ("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.MENUID+" "+Types.INTEGER+", "+Fields.FOODID+" "+Types.INTEGER+")");
        db.execSQL("create table " + Tables.TABLE_NAME_LUNCH_WEEK + "("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.WEEKID+" "+Types.INTEGER+", "+Fields.DATE+" "+Types.DATE+", "+Fields.MON+" "+Types.INTEGER+", "+Fields.TUE+" "+Types.INTEGER+", "+Fields.WED+" "+Types.INTEGER+", "+Fields.THU+" "+Types.INTEGER+", "+Fields.FRI+" "+ Types.INTEGER+", "+Fields.SAT+" "+Types.INTEGER+", "+Fields.SUN+" "+Types.INTEGER+")");
        db.execSQL("create table " + Tables.TABLE_NAME_ITEMTRANSLATION+ "("+Fields.ID+" "+ Types.INTEGER+" PRIMARY KEY , "+Fields.IDITEMTRANSLATION+" "+ Types.INTEGER+", "+Fields.NAME+" "+Types.TEXT+" , "+ Fields.TYPE+" "+Types.TEXT+", "+ Fields.LANGUAGEID+" "+Types.TEXT+", "+Fields.DESCRIPTION+" "+Types.TEXT+")");
        db.execSQL("create table " + Tables.TABLE_NAME_NEWS_TYPE+ "("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.TEXT+")");
        db.execSQL("create table " + Tables.TABLE_NAME_NEWS+ "("+Fields.ID+" "+Types.INTEGER+" PRIMARY KEY, "+Fields.NAME+" "+Types.TEXT+", "+Fields.DESCRIPTION+" "+Types.TEXT+", "+Fields.IMAGE+" "+ Types.TEXT+", "+Fields.IDNEWS+" "+Types.INTEGER+")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+ Tables.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_BEERS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_ALBUMS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_IMAGES);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_FOOD_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_FOOD);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_MENU_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_MENU);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_LUNCH_WEEK);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_MENU_CONTENT);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_ITEMTRANSLATION);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_NEWS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TABLE_NAME_NEWS_TYPE);

        onCreate(db);
    }

    public void open() throws SQLException{
        db = this.getWritableDatabase();
    }

    public void deleteRecords(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(tableName, "", null);
    }


    public void closeDB(){
        this.close();
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

}
