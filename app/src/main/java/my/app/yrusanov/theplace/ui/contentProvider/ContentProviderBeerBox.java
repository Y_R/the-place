package my.app.yrusanov.theplace.ui.contentProvider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.Toast;

import my.app.yrusanov.theplace.ui.DB.DBHelper;
import my.app.yrusanov.theplace.ui.DB.Tables;

import java.util.HashMap;

/**
 * Created by y.rusanov on 2017-1-4.
 */

public class ContentProviderBeerBox extends ContentProvider{

    static final String PROVIDER_NAME = "my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox";

    public static final Uri CONTENT_URI_BEERCATEGPRY = Uri.parse("content://" + PROVIDER_NAME + "/"+ Tables.TABLE_NAME+"");
    public static final Uri CONTENT_URI_BEER = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_BEERS+"");
    public static final Uri CONTENT_URI_FOOD = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_FOOD+"");
    public static final Uri CONTENT_URI_FOODCATEGORY = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_FOOD_CATEGORY+"");
    public static final Uri CONTENT_URI_IMAGES = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_IMAGES+"");
    public static final Uri CONTENT_URI_LUNCHWEEK = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_LUNCH_WEEK+"");
    public static final Uri CONTENT_URI_MENU = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_MENU+"");
    public static final Uri CONTENT_URI_MENUCONTENT = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_MENU_CONTENT+"");
    public static final Uri CONTENT_URI_MENUTYPE = Uri.parse("content://" + PROVIDER_NAME + "/"+ Tables.TABLE_NAME_MENU_TYPE+"");
    public static final Uri CONTENT_URI_ALBUMS = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_ALBUMS+"");
    public static final Uri CONTENT_URI_NEWS = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_NEWS+"");
    public static final Uri CONTENT_URI_NEWS_TYPE = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_NEWS_TYPE+"");
    public static final Uri CONTENT_URI_ITEMTRANSLATION = Uri.parse("content://" + PROVIDER_NAME + "/"+Tables.TABLE_NAME_ITEMTRANSLATION+"");

    public static final String and_cursor = "vnd.android.cursor.dir/";

    static final int BEERSCATEGORIES = 1;
    static final int BEERS = 2;
    static final int ALBUMS = 3;
    static final int IMAGES = 4;
    static final int FOOD = 5;
    static final int FOODCATEGORY = 6;
    static final int LUNCHWEEK = 7;
    static final int MENU = 8;
    static final int MENUCONTENT = 9;
    static final int MENUTYPE = 10;
    static final int NEWS = 11;
    static final int NEWSTYPE= 12;
    static final int ITEMTRANSLATION = 13;

    private static HashMap<String, String> values;
    static  final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME,BEERSCATEGORIES);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_BEERS,BEERS);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_ALBUMS,ALBUMS);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_IMAGES,IMAGES);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_FOOD,FOOD);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_FOOD_CATEGORY,FOODCATEGORY);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_LUNCH_WEEK,LUNCHWEEK);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_MENU,MENU);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_MENU_CONTENT,MENUCONTENT);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_MENU_TYPE,MENUTYPE);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_NEWS,NEWS);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_NEWS_TYPE,NEWSTYPE);
        uriMatcher.addURI(PROVIDER_NAME,Tables.TABLE_NAME_ITEMTRANSLATION,ITEMTRANSLATION);

    }
    private SQLiteDatabase sqlDB;
    private DBHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        sqlDB = dbHelper.getWritableDatabase();
        if(sqlDB != null){
            return true;
        }
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        Cursor cursor;

        switch (uriMatcher.match(uri)){
            case BEERSCATEGORIES:
                queryBuilder.setTables(Tables.TABLE_NAME);
                break;
            case BEERS:
                queryBuilder.setTables(Tables.TABLE_NAME_BEERS);
                break;
            case ALBUMS:
                queryBuilder.setTables(Tables.TABLE_NAME_ALBUMS);
                break;
            case IMAGES:
                queryBuilder.setTables(Tables.TABLE_NAME_IMAGES);
                break;
            case FOOD:
                queryBuilder.setTables(Tables.TABLE_NAME_FOOD);
                break;
            case FOODCATEGORY:
                queryBuilder.setTables(Tables.TABLE_NAME_FOOD_CATEGORY);
                break;
            case LUNCHWEEK:
                queryBuilder.setTables(Tables.TABLE_NAME_LUNCH_WEEK);
                break;
            case MENU:
                queryBuilder.setTables(Tables.TABLE_NAME_MENU);
                break;
            case MENUCONTENT:
                queryBuilder.setTables(Tables.TABLE_NAME_MENU_CONTENT);
                break;
            case MENUTYPE:
                queryBuilder.setTables(Tables.TABLE_NAME_MENU_TYPE);
                break;
            case NEWS:
                queryBuilder.setTables(Tables.TABLE_NAME_NEWS);
                break;
            case NEWSTYPE:
                queryBuilder.setTables(Tables.TABLE_NAME_NEWS_TYPE);
                break;
            case ITEMTRANSLATION:
                queryBuilder.setTables(Tables.TABLE_NAME_ITEMTRANSLATION);
                break;

            default:
                throw new IllegalArgumentException("Unkwon URI "+uri);
        }
        queryBuilder.setProjectionMap(values);
        int cnt;
        if(selection != null){
            Cursor cursorCount = sqlDB.rawQuery("SELECT * FROM "+queryBuilder.getTables()+" WHERE "+selection+"",null);
            cnt = cursorCount.getCount();
        }
        else{
            Cursor cursorCount = sqlDB.rawQuery("SELECT * FROM "+queryBuilder.getTables()+"",null);
            cnt = cursorCount.getCount();
        }

        if(cnt != 0){
            cursor = queryBuilder.query(sqlDB, projection, selection, selectionArgs, null,null,sortOrder);
            cursor.setNotificationUri(getContext().getContentResolver(),uri);
            return cursor;
        }
        else
            return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            case BEERSCATEGORIES:
                return and_cursor+Tables.TABLE_NAME;
            case BEERS:
                return and_cursor+Tables.TABLE_NAME_BEERS;
            case ALBUMS:
                return and_cursor+Tables.TABLE_NAME_ALBUMS;
            case IMAGES:
                return and_cursor+Tables.TABLE_NAME_IMAGES;
            case FOOD:
                return and_cursor+Tables.TABLE_NAME_FOOD;
            case FOODCATEGORY:
                return and_cursor+Tables.TABLE_NAME_FOOD_CATEGORY;
            case LUNCHWEEK:
                return and_cursor+Tables.TABLE_NAME_LUNCH_WEEK;
            case MENU:
                return and_cursor+Tables.TABLE_NAME_MENU;
            case MENUCONTENT:
                return and_cursor+Tables.TABLE_NAME_MENU_CONTENT;
            case MENUTYPE:
                return and_cursor+Tables.TABLE_NAME_MENU_TYPE;
            case NEWS:
                return and_cursor+Tables.TABLE_NAME_NEWS;
            case NEWSTYPE:
                return and_cursor+Tables.TABLE_NAME_NEWS_TYPE;
            case ITEMTRANSLATION:
                return and_cursor+Tables.TABLE_NAME_ITEMTRANSLATION;

            default:
                throw new IllegalArgumentException("Unsupported URI "+uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        long rowID;
        Uri _uri;

        switch (uriMatcher.match(uri)) {
            case BEERSCATEGORIES:
                rowID = sqlDB.insert(Tables.TABLE_NAME, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_BEERCATEGPRY, rowID);
                break;
            case BEERS:
                rowID = sqlDB.insert(Tables.TABLE_NAME_BEERS, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_BEER, rowID);
                break;
            case ALBUMS:
                rowID = sqlDB.insert(Tables.TABLE_NAME_ALBUMS, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_ALBUMS, rowID);
                break;
            case IMAGES:
                rowID = sqlDB.insert(Tables.TABLE_NAME_IMAGES, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_IMAGES, rowID);
                break;
            case FOOD:
                rowID = sqlDB.insert(Tables.TABLE_NAME_FOOD, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_FOOD, rowID);
                break;
            case FOODCATEGORY:
                rowID = sqlDB.insert(Tables.TABLE_NAME_FOOD_CATEGORY, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_FOODCATEGORY, rowID);
                break;
            case LUNCHWEEK:
                rowID = sqlDB.insert(Tables.TABLE_NAME_LUNCH_WEEK, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_LUNCHWEEK, rowID);
                break;
            case MENU:
                rowID = sqlDB.insert(Tables.TABLE_NAME_MENU, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_MENU, rowID);
                break;
            case MENUCONTENT:
                rowID = sqlDB.insert(Tables.TABLE_NAME_MENU_CONTENT, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_MENUCONTENT, rowID);
                break;
            case MENUTYPE:
                rowID = sqlDB.insert(Tables.TABLE_NAME_MENU_TYPE, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_MENUTYPE, rowID);
                break;
            case NEWS:
                rowID = sqlDB.insert(Tables.TABLE_NAME_NEWS, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_NEWS, rowID);
                break;
            case NEWSTYPE:
                rowID = sqlDB.insert(Tables.TABLE_NAME_NEWS_TYPE, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_NEWS_TYPE, rowID);
                break;
            case ITEMTRANSLATION:
                rowID = sqlDB.insert(Tables.TABLE_NAME_ITEMTRANSLATION, null, values);
                _uri = ContentUris.withAppendedId(CONTENT_URI_ITEMTRANSLATION, rowID);
                break;
            default:
                throw new IllegalArgumentException("Unkwon URI " + uri);
        }

        if(rowID > 0){
            getContext().getContentResolver().notifyChange(_uri,null);
            return _uri;
        }
        else{
            Toast.makeText(getContext(), "Row Inserted Failed",Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        int rowID;
        Uri _uri;

        switch (uriMatcher.match(uri)) {
            case BEERSCATEGORIES:
                rowID = sqlDB.delete(Tables.TABLE_NAME, null,null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_BEERCATEGPRY, rowID);
                break;
            case BEERS:
                rowID = sqlDB.delete(Tables.TABLE_NAME_BEERS, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_BEER, rowID);
                break;
            case ALBUMS:
                rowID = sqlDB.delete(Tables.TABLE_NAME_ALBUMS, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_ALBUMS, rowID);
                break;
            case IMAGES:
                rowID = sqlDB.delete(Tables.TABLE_NAME_IMAGES, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_IMAGES, rowID);
                break;
            case FOOD:
                rowID = sqlDB.delete(Tables.TABLE_NAME_FOOD, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_FOOD, rowID);
                break;
            case FOODCATEGORY:
                rowID = sqlDB.delete(Tables.TABLE_NAME_FOOD_CATEGORY, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_FOODCATEGORY, rowID);
                break;
            case LUNCHWEEK:
                rowID = sqlDB.delete(Tables.TABLE_NAME_LUNCH_WEEK, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_LUNCHWEEK, rowID);
                break;
            case MENU:
                rowID = sqlDB.delete(Tables.TABLE_NAME_MENU, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_MENU, rowID);
                break;
            case MENUCONTENT:
                rowID = sqlDB.delete(Tables.TABLE_NAME_MENU_CONTENT, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_MENUCONTENT, rowID);
                break;
            case MENUTYPE:
                rowID = sqlDB.delete(Tables.TABLE_NAME_MENU_TYPE, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_MENUTYPE, rowID);
                break;
            case NEWS:
                rowID = sqlDB.delete(Tables.TABLE_NAME_NEWS, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_NEWS, rowID);
                break;
            case NEWSTYPE:
                rowID = sqlDB.delete(Tables.TABLE_NAME_NEWS_TYPE, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_NEWS_TYPE, rowID);
                break;
            case ITEMTRANSLATION:
                rowID = sqlDB.delete(Tables.TABLE_NAME_ITEMTRANSLATION, null, null);
                _uri = ContentUris.withAppendedId(CONTENT_URI_ITEMTRANSLATION, rowID);
                break;
            default:
                throw new IllegalArgumentException("Unkwon URI " + uri);
        }

        if(rowID > 0){
            getContext().getContentResolver().notifyChange(_uri,null);
            return rowID;
        }
        else{
            //Toast.makeText(getContext(), "Row Deleted Failed",Toast.LENGTH_SHORT).show();
            return 0;
        }

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
