package my.app.yrusanov.theplace.ui.tables;

/**
 * Created by y.rusanov on 2017-3-8.
 */

public class NewsType_table {

    private int id;
    private String name;


    public NewsType_table(int _id, String _name){
        this.id = _id;
        this.name = _name;
    }

    public NewsType_table(String _name){
        this.name = _name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
