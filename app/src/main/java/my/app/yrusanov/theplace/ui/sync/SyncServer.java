package my.app.yrusanov.theplace.ui.sync;

import android.os.RemoteException;

import my.app.yrusanov.theplace.ui.DB.FireBird.DBOperationFB;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;

/**
 * Created by y.rusanov on 2017-1-4.
 */

public class SyncServer implements Sync {

    private DBOperationFB dbOperationFB;

    public SyncServer(){

    }

    @Override
    public void BeerCategorySync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void BeerSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void MenuTypeSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void MenuSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void MenuContentSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void FoodCategorySync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void FoodSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void LunchWeekSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void AlbumSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void Images(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void ItemTranslationSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void NewsTypeSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void NewsSync(Socket client) throws IOException, SQLException, RemoteException {

    }
}
