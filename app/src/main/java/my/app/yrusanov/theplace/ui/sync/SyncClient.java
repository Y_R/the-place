package my.app.yrusanov.theplace.ui.sync;

import android.content.ContentProviderClient;
import android.content.Context;
import android.os.RemoteException;

import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.image.ImagesFile;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by y.rusanov on 2017-1-4.
 */

public class SyncClient implements Sync {

    private Context context;
    private DBOperation dbOperation;
    private ContentProviderClient provider;
    public SyncClient(Context _context, ContentProviderClient contentProviderClient){
        context = _context;
        provider = contentProviderClient;
    }

    public SyncClient(Context _context){
        context = _context;
    }

    public String getImage(ByteString message, String _name, String _type){
        ImagesFile imagesFile = null;
        byte[] dyteImage = new byte[message.size()];
        message.copyTo(dyteImage, 0);

        if (dyteImage.length > 0) {
            HashMap<String, byte[]> mapValue = new HashMap<>();
            mapValue.put(_name, dyteImage);
            imagesFile = new ImagesFile(context, mapValue, _type);
        }
        if(imagesFile != null){
            return imagesFile.getFilePath();
        }
        else
            return "";
    }

    @Override
    public void BeerCategorySync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void BeerSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void MenuTypeSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void MenuSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void MenuContentSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void FoodCategorySync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void FoodSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void LunchWeekSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void AlbumSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void Images(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void ItemTranslationSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void NewsTypeSync(Socket client) throws IOException, SQLException, RemoteException {

    }

    @Override
    public void NewsSync(Socket client) throws IOException, SQLException, RemoteException {

    }
}
