package my.app.yrusanov.theplace.ui.activities;

/*
adb -d shell
adb su
BeerBox.db > /storage/sdcard0/beerbox.db

 */

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.net.Socket;
import java.util.Locale;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.AddData;
import my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{

    private Socket client;
    private final int JOB_ID = 999;
    public static  final String PREFS_NAME = "MyPrefsFile";
    Locale myLocale;
    int choosenLang;
    public boolean firstStart = true;
    Account mAccount = null;
    public static final String ACCOUNT = "BeerBox";
    public static final String AUTHORITY = "my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox";
    public static final String ACCOUNT_TYPE = "example.com";
    public String language;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        language  = prefs.getString("list_preferences","en");
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        this.getApplicationContext().getResources().updateConfiguration(config, null);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle(R.string.ActionTitle);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        //this.deleteDatabase("BeerBox.db");
        Cursor cursor = this.getContentResolver().query(ContentProviderBeerBox.CONTENT_URI_BEERCATEGPRY,null,null,null,null);
        if(cursor == null){
            AddData addData = new AddData(this);
        }

        //// TODO: 2017-7-13 
        //mAccount = CreateSyncAccount(this);
        
        
        //ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
        //FirebaseInstanceId.getInstance().getToken();
        
        //// TODO: 2017-7-13  
        //FirebaseMessaging.getInstance().subscribeToTopic("news");
    }

    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
            */
            //ContentResolver.setIsSyncable(newAccount,AUTHORITY,1);
            ContentResolver.setSyncAutomatically(newAccount, AUTHORITY,true);
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
            Log.i("Account", "Exists");
        }
        return newAccount;
    }


    public void clickBeers(View v){
        Intent intent = new Intent(this, Beers.class);
        startActivity(intent);
        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    public void clickMenu(View v){
        Intent intent = new Intent(this, Menu.class);
        startActivity(intent);
        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    public void clickGallery(View v){
        Intent intent = new Intent(this, Gallery.class);
        startActivity(intent);
        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    public void clickLunch(View v){
        Intent intent = new Intent(this, Lunch.class);
        startActivity(intent);
        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    public void clickNews(View view) {
        Intent intent = new Intent(this, News.class);
        startActivity(intent);
    }
    public static String FACEBOOK_URL = "https://www.facebook.com/BeerBox-GrillBeer-456124207909007/";
    public static String FACEBOOK_PAGE_ID = "BeerBox-GrillBeer-456124207909007/";

    public String getFacebookPageURL(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_URL; //normal web url
        }
    }

    public void clickCall(View view) {
        /*
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Резервация: ");
        final String numberCall = "+359879824000";
        alertDialog.setMessage("Направи резервация сега: "+numberCall+"");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "ОБАДИ СЕ",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:"+numberCall+""));
                        startActivity(callIntent);
                    }
                });
        alertDialog.show();
        */
    }

    public void clickFB(View view) {
        /*
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL(this);
        facebookIntent.setData(Uri.parse(facebookUrl));
        startActivity(facebookIntent);
        */
    }

    public void clickAddressMap(View view) {
        /*
        Intent intent = new Intent(this, MapPane.class);
        startActivity(intent);
        */

    }

    public void clickSettings(View view) {

        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);

    }

    public void clickStreetView(View view) {
        /*
        Intent intent = new Intent(this, WebStreetView.class);
        startActivity(intent);
        */
    }
}