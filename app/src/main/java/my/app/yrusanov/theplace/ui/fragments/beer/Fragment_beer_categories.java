package my.app.yrusanov.theplace.ui.fragments.beer;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.adapters.Beers.RecyclerAdapterBeers;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_beer_categories extends android.support.v4.app.Fragment {

    RecyclerView recyclerView;
    RecyclerAdapterBeers adapter;
    Fragment_beer fragment_beer;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment fragmentBeer;
    View view;
    LinearLayoutManager mLayoutManager;

    public Fragment_beer_categories() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_beer_categories, container, false);
        getActivity().setTitle(this.getActivity().getIntent().getStringExtra("CategoryName"));
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_beers);
        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(mLayoutManager);


        adapter = new RecyclerAdapterBeers(this.getActivity().getBaseContext(),this.getActivity().getIntent().getIntExtra("IdCategory",0));

        recyclerView.setAdapter(adapter);
        fragment_beer = new Fragment_beer();
        fragmentManager = getFragmentManager();

        adapter.SetOnItemClickListener( new RecyclerAdapterBeers.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                TextView textView = (TextView) view.findViewById(R.id.textViewName);
                TextView textviewId = (TextView)view.findViewById(R.id.textViewID_Beer);
                Bundle bundle = new Bundle();
                bundle.putString("Name",textView.getText().toString());
                bundle.putInt("Id",Integer.valueOf(textviewId.getText().toString()));
                fragment_beer.setArguments(bundle);
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left,R.animator.enter_from_left, R.animator.exit_to_right);
                fragmentTransaction.replace(R.id.fragment_container,fragment_beer);
                fragmentTransaction.addToBackStack("fragment_beer_categories");
                fragmentTransaction.commit();
            }
        });

        return view;
    }

}
