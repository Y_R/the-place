package my.app.yrusanov.theplace.ui.activities;


import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.adapters.Beers.RecyclerAdapterBeersCategories;

public class Beers extends AppCompatActivity{
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    RecyclerView recyclerView;
    ContentResolver resolver;

    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME+"");
    RecyclerAdapterBeersCategories adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beers);
        //getActionBar().setTitle("Бири");
        getSupportActionBar().setTitle(R.string.Categories);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_beer_categories);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(gridLayoutManager);


        adapter = new RecyclerAdapterBeersCategories(this);

        recyclerView.setAdapter(adapter);


        adapter.SetOnItemClickListener(new RecyclerAdapterBeersCategories.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView textView_beer_category = (TextView) view.findViewById(R.id.textView_beer_categories);
                TextView textView_beer_IdCategory = (TextView) view.findViewById(R.id.textView_beer_IdCategory);
                String categoryName;
                Intent intent = new Intent(view.getContext(), fragment_activity.class);
                intent.putExtra("activity", "Beers_categories");

                resolver = getContentResolver();

                String whereClause = ""+ Fields.ID+" = "+textView_beer_IdCategory.getText().toString()+"";

                Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
                cursor.moveToFirst();
                int idCategory = cursor.getInt(0); //Integer.parseInt(textView_beer_IdCategory.getText().toString());
                categoryName = textView_beer_category.getText().toString();
                intent.putExtra("IdCategory", idCategory);
                intent.putExtra("CategoryName", categoryName);
                startActivity(intent);
                overridePendingTransition(R.animator.enter_from_right, R.animator.exit_to_left);
            }
        });
    }
}
