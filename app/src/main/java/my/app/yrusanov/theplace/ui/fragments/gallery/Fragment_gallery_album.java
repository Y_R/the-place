package my.app.yrusanov.theplace.ui.fragments.gallery;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.adapters.Images.*;
/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_gallery_album extends android.support.v4.app.Fragment{

    RecyclerView recyclerView;
    //RecyclerAdapter_images adapter;
    RecyclerAdapterImages adapter;
    Fragment_image fragment_image;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment fragmentImage;
    View view;

    public Fragment_gallery_album() {
        // Required empty public constructor
    }

    GridView gv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_gallery_album, container, false);
        getActivity().setTitle(this.getActivity().getIntent().getStringExtra("AlbumName"));
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_gallery_image);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(view.getContext(),2);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new RecyclerAdapterImages(this.getActivity().getBaseContext(),this.getActivity().getIntent().getIntExtra("IdAlbum",0));

        recyclerView.setAdapter(adapter);
        fragmentManager = getFragmentManager();
        fragment_image = new Fragment_image();
        adapter.SetOnItemClickListener(new RecyclerAdapterImages.OnItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {

                TextView textView = (TextView) view.findViewById(R.id.textView_image);
                Bundle bundle = new Bundle();
                bundle.putString("Name",textView.getText().toString());
                fragment_image.setArguments(bundle);
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left,R.animator.enter_from_left, R.animator.exit_to_right);
                //fragmentTransaction.setCustomAnimations(R.animator.slidein,R.animator.slideout,R.animator.slidein,R.animator.slideout);
                fragmentTransaction.replace(R.id.fragment_container,fragment_image);
                fragmentTransaction.addToBackStack("fragment_gallery_album");
                fragmentTransaction.commit();
            }
        });

        return view;
    }

}
