package my.app.yrusanov.theplace.ui.adapters.Images;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.tables.Image;
import my.app.yrusanov.image.ImagesFile;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-9-2.
 */
public class RecyclerAdapterImages extends RecyclerView.Adapter<RecyclerAdapterImages.ViewHolder>{

    ArrayList<Image> elements;
    OnItemClickListener mItemClickListener;
    Image image;
    String path;
    ContentResolver resolver;
    public String language;
    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_IMAGES+"");

    public RecyclerAdapterImages(Context _context, int IDAlbum){

        elements = new ArrayList<>();
        Bitmap bitmap;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(_context);
        language  = prefs.getString("list_preferences","en");
        String contentName;
        try {
            if(IDAlbum != 0){
                resolver = _context.getContentResolver();
                String whereClause = ""+ Fields.IDALBUM+" = "+IDAlbum+"";
                Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);

                if(cursor != null){
                    cursor.moveToFirst();

                    do {
                        contentName = DBOperation.getTranslation(_context, cursor.getInt(0), Types.IMAGES,language,"Name");
                        if(contentName.equals("")) {
                            contentName = cursor.getString(1);
                        }
                        bitmap = ImagesFile.getImageByType(cursor.getString(1), Types.IMAGES);
                        if(bitmap != null){
                            elements.add(new Image(cursor.getInt(0), contentName, cursor.getString(2),bitmap, cursor.getInt(4)));
                        }
                        else {
                            bitmap = BitmapFactory.decodeResource(_context.getResources(),R.drawable.image);
                            elements.add(new Image(cursor.getInt(0), contentName, cursor.getString(2),bitmap, cursor.getInt(4)));
                        }
                    } while (cursor.moveToNext());
                }

            }
        }
        catch(ArrayIndexOutOfBoundsException exception) {
            exception.printStackTrace();
        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_gallery_image,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        image = elements.get(position);
        holder.imageViewId.setImageBitmap(image.getImage());
        holder.textView_image.setText(image.getName());
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        TextView textView_image;
        ImageView imageViewId;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            textView_image = (TextView)view.findViewById(R.id.textView_image);
            imageViewId =(ImageView)view.findViewById(R.id.imageViewId);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.OnItemClick(v, getAdapterPosition());
        }
    }

    public interface OnItemClickListener{
        public void OnItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener){
        this.mItemClickListener = mItemClickListener;
    }
}
