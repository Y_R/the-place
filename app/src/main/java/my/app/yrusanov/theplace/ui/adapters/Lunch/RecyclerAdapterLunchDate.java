package my.app.yrusanov.theplace.ui.adapters.Lunch;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.tables.Food;
import my.app.yrusanov.theplace.ui.tables.FoodCategory;

import java.io.File;
import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by y.rusanov on 2017-1-28.
 */

public class RecyclerAdapterLunchDate extends RecyclerView.Adapter<RecyclerAdapterLunchDate.ViewHolder> {



    FoodCategory foodCategory;
    Food food;
    ArrayList<FoodCategory> elements;
    ArrayList<Food> elementsFood;
    ContentResolver resolver;
    String path;
    public String language;
    static final Uri CONTENT_URL_FOODCATEGORY = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_FOOD_CATEGORY+"");
    static final Uri CONTENT_URL_FOOD = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_FOOD+"");
    static final Uri CONTENT_URL_MENUCONTENT = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_MENU_CONTENT+"");
    TreeSet<Integer> setIdFoodCategory = new TreeSet <Integer>();
    public RecyclerAdapterLunchDate(Context _context, int MenuId) {
        elements = new ArrayList<>();
        elementsFood = new ArrayList<>();
        resolver = _context.getContentResolver();
        String contentName;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(_context);
        language  = prefs.getString("list_preferences","en");
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/Food";
        String whereClause = ""+ Fields.MENUID+" = "+MenuId+"";
        Cursor cursor = resolver.query(CONTENT_URL_MENUCONTENT,null,whereClause,null,null);
        if(cursor != null) {
            cursor.moveToFirst();
            do {
                File imageFile;
                String whereClauseFood = "" + Fields.ID + " = " + cursor.getInt(2) + "";
                String orderByFood = ""+Fields.IDCATEGORY+" ASC";
                Cursor cursorFood = resolver.query(CONTENT_URL_FOOD, null, whereClauseFood, null, null);

                imageFile = new File(path + "/"+cursor.getString(1)+".jpg");
                if(imageFile.exists()){
                    if(cursorFood != null){
                        cursorFood.moveToFirst();
                        contentName = DBOperation.getTranslation(_context, cursorFood.getInt(0), Types.FOOD,language,"Name");
                        if(contentName.equals("")) {
                            contentName = cursorFood.getString(1);
                        }
                        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                        elementsFood.add(new Food(cursorFood.getInt(0), contentName, cursorFood.getString(2), cursorFood.getFloat(3), bitmap, cursorFood.getInt(5)));
                        setIdFoodCategory.add(cursorFood.getInt(5));
                    }
                }
                else {
                    if(cursorFood != null){
                        cursorFood.moveToFirst();
                        contentName = DBOperation.getTranslation(_context, cursorFood.getInt(0), Types.FOOD,language,"Name");
                        if(contentName.equals("")) {
                            contentName = cursorFood.getString(1);
                        }
                        Bitmap bitmap = BitmapFactory.decodeResource(_context.getResources(),R.drawable.logo);
                        elementsFood.add(new Food(cursorFood.getInt(0), contentName, cursorFood.getString(2), cursorFood.getFloat(3),bitmap, cursorFood.getInt(5)));
                        setIdFoodCategory.add(cursorFood.getInt(5));
                    }
                }
            }
            while (cursor.moveToNext());
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_lunch_date_menu, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String lunchMenu = "";
        for(Food food : elementsFood){
            lunchMenu += Html.fromHtml(""+food.getName()+" "+food.getWeight()+" "+food.getPrice()+" лв.<br />");
        }
        holder.textViewDescription.setText(lunchMenu);
    }

    @Override
    public int getItemCount() {
        return 1;//elements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        TextView textViewDescription;
        ImageView imageViewFoodLunch;
        public ViewHolder(View view) {
            super(view);
            textViewDescription = (TextView) view.findViewById(R.id.textViewDescriptionLunch);
        }
    }
}
