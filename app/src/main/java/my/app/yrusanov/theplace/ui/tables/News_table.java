package my.app.yrusanov.theplace.ui.tables;

import android.graphics.Bitmap;

/**
 * Created by y.rusanov on 2017-3-8.
 */

public class News_table {

    private int id;
    private String name;
    private String description;
    private int idNews;
    private Bitmap image;

    public News_table(int _id, String _name, String _description, Bitmap _image, int _idNews){
        this.id = _id;
        this.name = _name;
        this.description = _description;
        this.image = _image;
        this.idNews = _idNews;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdNews() {
        return idNews;
    }

    public void setIdNews(int idNews) {
        this.idNews = idNews;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
