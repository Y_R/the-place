package my.app.yrusanov.theplace.ui.adapters.Lunch;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.tables.Menu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by y.rusanov on 2016-8-11.
 */
public class RecyclerAdapterLunch extends RecyclerView.Adapter<RecyclerAdapterLunch.ViewHolder>{



    private final ArrayList<Menu> elements;
    ContentResolver resolver;
    Menu menu;
    OnItemClickListener mItemClickListener;
    static final Uri CONTENT_URL_MENUTYPE = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_MENU_TYPE+"");
    static final Uri CONTENT_URL_MENU = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_MENU+"");
    public String language;
    int idMenuType;
    String DATE_FORMAT_NOW = "dd-MM-yyyy";
    Date date;
    String stringDate;
    SimpleDateFormat sdf;
    public RecyclerAdapterLunch(Context _context) throws ParseException {
        elements = new ArrayList<Menu>();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(_context);
        language  = prefs.getString("list_preferences","en");

        sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        resolver = _context.getContentResolver();
        String whereClause = ""+ Fields.NAME+" = 'Обедно меню'";
        Cursor cursor = resolver.query(CONTENT_URL_MENUTYPE,null,whereClause,null,null);
        if(cursor != null){
            cursor.moveToFirst();
            idMenuType = cursor.getInt(0);
        }
        String orderBy =  Fields.DATE+ " DESC";
        whereClause = ""+ Fields.TYPE+" = "+idMenuType+"";
        cursor = resolver.query(CONTENT_URL_MENU,null,whereClause,null,orderBy);

        if(cursor != null){
            cursor.moveToFirst();
            do{
                stringDate = cursor.getString(1);
                date = sdf.parse(stringDate);
                elements.add(new Menu(cursor.getInt(0),date, cursor.getInt(2)));
            }while (cursor.moveToNext());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_lunch_menu, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //final String rowData = elements.get(position);
        menu = elements.get(position);
        holder.textViewDescription.setText(R.string.LunchMenu);
        date = menu.getDate();
        holder.textViewDate.setText(sdf.format(date));
        holder.textViewIdMenu.setText(String.valueOf(menu.getID()));

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int reslutDay = calendar.get(Calendar.DAY_OF_WEEK);
        String dayName;
        if(language.equals("bg")){
            switch (reslutDay) {
                case Calendar.MONDAY:
                    dayName = "Понеделник";
                    break;
                case Calendar.TUESDAY:
                    dayName = "Вторник";
                    break;
                case Calendar.WEDNESDAY:
                    dayName = "Сряда";
                    break;
                case Calendar.THURSDAY:
                    dayName = "Четвъртък";
                    break;
                case Calendar.FRIDAY:
                    dayName = "Петък";
                    break;
                default:
                    dayName = " - ";
            }
        }
        else{
            switch (reslutDay) {
                case Calendar.MONDAY:
                    dayName = "Monday";
                    break;
                case Calendar.TUESDAY:
                    dayName = "Tuesday";
                    break;
                case Calendar.WEDNESDAY:
                    dayName = "Wednesday";
                    break;
                case Calendar.THURSDAY:
                    dayName = "Thursday";
                    break;
                case Calendar.FRIDAY:
                    dayName = "Friday";
                    break;
                default:
                    dayName = " - ";
            }
        }

        holder.textViewDay.setText(dayName);

    }

    @Override
    public int getItemCount() {
        return elements.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener
    {
        TextView textViewDate;
        TextView textViewDescription;
        TextView textViewIdMenu;
        TextView textViewDay;
        public ViewHolder(View view){
            super(view);

            view.setOnClickListener(this);
            textViewDate = (TextView) view.findViewById(R.id.textView_lunch_menu_date);
            textViewDescription = (TextView) view.findViewById(R.id.textView_lunch_menu_description);
            textViewIdMenu = (TextView)view.findViewById(R.id.textViewIdMenu);
            textViewDay = (TextView)view.findViewById(R.id.textView_lunch_menu_day);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface  OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }



}
