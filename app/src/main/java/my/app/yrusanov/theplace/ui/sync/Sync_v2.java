package my.app.yrusanov.theplace.ui.sync;

import android.os.RemoteException;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by y.rusanov on 2017-6-19.
 */

public interface Sync_v2 {
    HashMap<Object, Integer> setLastRecId() throws RemoteException;
    String getImage(ByteString message, String _name, String _type);
    void BeerCategorySync() throws IOException, SQLException, RemoteException;
    void BeerSync() throws IOException, SQLException, RemoteException;
    void MenuTypeSync() throws IOException, SQLException, RemoteException;
    void MenuSync() throws IOException, SQLException, RemoteException;
    void MenuContentSync() throws IOException, SQLException, RemoteException;
    void FoodCategorySync() throws IOException, SQLException, RemoteException;
    void FoodSync() throws IOException, SQLException, RemoteException;
    void LunchWeekSync() throws IOException, SQLException, RemoteException;
    void AlbumSync() throws IOException, SQLException, RemoteException;
    void Images() throws IOException, SQLException, RemoteException;
    void ItemTranslationSync() throws IOException, SQLException, RemoteException;
    void NewsTypeSync() throws IOException, SQLException, RemoteException;
    void NewsSync() throws IOException, SQLException, RemoteException;
    void sendLastRecId(Socket client,HashMap<Object, Integer> mapLastRecId) throws IOException;
    void sendProtoMessage() throws RemoteException;
    HashMap<Object, Integer> getLastRecId() throws IOException, ClassNotFoundException;
    void sync(Socket client) throws IOException, SQLException, RemoteException;
}
