package my.app.yrusanov.theplace.ui.fragments.lunch;



import android.content.ContentResolver;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.adapters.Lunch.RecyclerAdapterLunch;

import java.text.ParseException;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_lunch_menu extends Fragment {

    RecyclerView recyclerView;

    Fragment_lunch_date_menu fragment_lunch_date_menu;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    View view;
    LinearLayoutManager mLayoutManager;
    RecyclerAdapterLunch adapter;
    ContentResolver resolver;
    static final Uri CONTENT_URL_MENU = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_MENU+"");


    public Fragment_lunch_menu() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment__lunch_menu, container, false);
        //recyclerView.setHasFixedSize(true);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_lunch_menu);

        //setting the layout manager
        mLayoutManager = new GridLayoutManager(view.getContext(),2);
        recyclerView.setLayoutManager(mLayoutManager);
        //setting the items decoration

        //setting the adapter
        try {
            adapter = new RecyclerAdapterLunch(view.getContext());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        recyclerView.setAdapter(adapter);
        fragment_lunch_date_menu = new Fragment_lunch_date_menu();
        fragmentManager = getFragmentManager();
        adapter.SetOnItemClickListener(new RecyclerAdapterLunch.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                TextView textView = (TextView) view.findViewById(R.id.textViewIdMenu);
                TextView textView1 = (TextView)view.findViewById(R.id.textView_lunch_menu_date);
                TextView textView2 = (TextView)view.findViewById(R.id.textView_lunch_menu_day);

                Bundle bundle = new Bundle();
                bundle.putString("Id",textView.getText().toString());
                bundle.putString("DateLunch", (String) textView1.getText());
                bundle.putString("DayLunch",(String) textView2.getText());
                fragment_lunch_date_menu.setArguments(bundle);

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left,R.animator.enter_from_left, R.animator.exit_to_right);
                //fragmentTransaction.setCustomAnimations(R.animator.fadein, R.animator.fadeout);
                fragmentTransaction.replace(R.id.fragment_container, fragment_lunch_date_menu);
                fragmentTransaction.addToBackStack("fragment_lunch_menu");
                fragmentTransaction.commit();
            }
        });

        return view;
    }

}
