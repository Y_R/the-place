package my.app.yrusanov.theplace.ui.adapters.Menu;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.tables.FoodCategory;
import my.app.yrusanov.image.ImagesFile;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-8-31.
 */
public class RecyclerAdapterMenuCategories extends RecyclerView.Adapter<RecyclerAdapterMenuCategories.ViewHolder> {

    ArrayList<FoodCategory> elements;
    FoodCategory foodCategory;
    OnItemClickListener mItemClickListener;
    public String language;
    ContentResolver resolver;
    String path;
    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_FOOD_CATEGORY+"");


    public RecyclerAdapterMenuCategories(Context _context){

        elements = new ArrayList<>();

        resolver = _context.getContentResolver();
        String contentName;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(_context);
        language  = prefs.getString("list_preferences","en");
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/BeerCategories";
        String[] projection = new String[]{"ID", "NAME"};
        Cursor cursor = resolver.query(CONTENT_URL,projection,null,null,null);
        if(cursor != null){
            cursor.moveToFirst();
            do{
                if(cursor.getInt(0) != 1){
                    contentName = DBOperation.getTranslation(_context, cursor.getInt(0), Types.FOODCATEGORIES,language,"Name");
                    if(contentName.equals("")) {
                        contentName = cursor.getString(1);
                    }
                    Bitmap bitmap = ImagesFile.getImageByType(cursor.getString(1), Types.FOODCATEGORIES);
                    if (bitmap != null){
                        elements.add(new FoodCategory(cursor.getInt(0),contentName, bitmap));
                    }
                    else {
                        bitmap = BitmapFactory.decodeResource(_context.getResources(),R.drawable.food);
                        elements.add(new FoodCategory(cursor.getInt(0),contentName, bitmap));
                    }
                }
            }while (cursor.moveToNext());
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_menu_categories,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        foodCategory = (FoodCategory) elements.get(position);
        holder.textView_menu_category.setText(foodCategory.getName());
        holder.textView_menu_IDcategory.setText(String.valueOf(foodCategory.getID()));
        holder.imageView_Categories.setImageBitmap(foodCategory.getImage());
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        TextView textView_menu_category;
        TextView textView_menu_IDcategory;
        ImageView imageView_Categories;
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView_menu_category = (TextView)itemView.findViewById(R.id.textView_menu_categories);
            textView_menu_IDcategory = (TextView)itemView.findViewById(R.id.textView_menu_IDcategories);
            imageView_Categories = (ImageView) itemView.findViewById(R.id.imageView_Categories);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface  OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }
}
