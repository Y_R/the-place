package my.app.yrusanov.theplace.ui.adapters.Menu;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox;
import my.app.yrusanov.theplace.ui.tables.Food;
import my.app.yrusanov.image.ImagesFile;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-9-2.
 */
public class RecyclerAdapterMenuItems extends RecyclerView.Adapter<RecyclerAdapterMenuItems.ViewHolder>{

    ArrayList<Food> elements;
    OnItemClickListener mItemClickListener;
    Food food;
    ContentResolver resolver;
    String path;
    public String language;
    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_FOOD+"");

    public RecyclerAdapterMenuItems(Context _context, int _IdCategory){

        elements = new ArrayList<>();
        String contentName;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(_context);
        language  = prefs.getString("list_preferences","en");
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/Food";
        try {
            if(_IdCategory != 0){
                resolver = _context.getContentResolver();
                String whereClause = " "+ Fields.IDCATEGORY+" = "+_IdCategory+" ";
                Cursor cursor = resolver.query(ContentProviderBeerBox.CONTENT_URI_FOOD,null,whereClause,null,null);
                if(cursor != null){
                    cursor.moveToFirst();
                    do {
                        contentName = DBOperation.getTranslation(_context, cursor.getInt(0), Types.FOOD,language,"Name");
                        if(contentName.equals("")) {
                            contentName = cursor.getString(1);
                        }
                        Bitmap bitmap = ImagesFile.getImageByType(cursor.getString(1), Types.FOOD);
                        if(bitmap != null) {
                            elements.add(new Food(cursor.getInt(0), contentName, cursor.getString(2), cursor.getFloat(3),bitmap, cursor.getInt(5)));
                        }
                        else{
                            bitmap = BitmapFactory.decodeResource(_context.getResources(),R.drawable.food);
                            elements.add(new Food(cursor.getInt(0), contentName, cursor.getString(2), cursor.getFloat(3), bitmap, cursor.getInt(5)));
                        }
                    } while (cursor.moveToNext());
                }

            }
        }
        catch(ArrayIndexOutOfBoundsException exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_menu_item,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(elements != null) {
            food = (Food) elements.get(position);
            holder.textView_item.setText(food.getName());
            holder.textView_menu_weight.setText(food.getWeight());
            holder.textView_menu_price.setText(String.valueOf(food.getPrice()));
            holder.imageView.setImageBitmap(food.getImage());
            holder.textView_menu_id.setText(String.valueOf(food.getID()));
        }
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        TextView textView_item, textView_menu_description, textView_menu_weight,textView_menu_price, textView_menu_id;
        ImageView imageView;
        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            textView_item = (TextView)view.findViewById(R.id.textViewName_MenuFood);
            textView_menu_price = (TextView)view.findViewById(R.id.textViewPrice_MenuFood);
            textView_menu_weight = (TextView)view.findViewById(R.id.textViewWeight_MenuFood);
            imageView = (ImageView)view.findViewById(R.id.imageProfile_MenuFood);
            textView_menu_id = (TextView)view.findViewById(R.id.textViewID_MenuFood);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.OnItemClick(v, getAdapterPosition());
        }
    }

    public interface OnItemClickListener{
        public void OnItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener){
        this.mItemClickListener = mItemClickListener;
    }
}
