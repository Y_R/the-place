package my.app.yrusanov.theplace.ui.activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.adapters.News.RecyclerAdapterNewsType;

/**
 * Created by y.rusanov on 2017-3-8.
 */

public class News extends AppCompatActivity {
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    RecyclerView recyclerView;
    ContentResolver resolver;

    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_NEWS_TYPE+"");
    RecyclerAdapterNewsType adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        //getActionBar().setTitle("Бири");
        getSupportActionBar().setTitle(R.string.Categories);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_news_type);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(gridLayoutManager);


        adapter = new RecyclerAdapterNewsType(this);

        recyclerView.setAdapter(adapter);


        adapter.SetOnItemClickListener(new RecyclerAdapterNewsType.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView textView_news_name = (TextView) view.findViewById(R.id.textView_news_name);
                TextView textView_news_id = (TextView) view.findViewById(R.id.textView_news_Idnews);
                String categoryName;
                Intent intent = new Intent(view.getContext(), fragment_activity.class);
                intent.putExtra("activity", "News_type");

                resolver = getContentResolver();

                String whereClause = ""+ Fields.NAME+" = '"+textView_news_name.getText().toString()+"'";
                Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
                cursor.moveToFirst();
                int idCategory = cursor.getInt(0); //Integer.parseInt(textView_beer_IdCategory.getText().toString());
                categoryName = cursor.getString(1);
                intent.putExtra("NewsId", idCategory);
                intent.putExtra("NewsName", categoryName);
                startActivity(intent);
                overridePendingTransition(R.animator.enter_from_right, R.animator.exit_to_left);
            }
        });
    }

}
