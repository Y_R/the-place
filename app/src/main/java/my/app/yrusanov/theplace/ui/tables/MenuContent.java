package my.app.yrusanov.theplace.ui.tables;

/**
 * Created by y.rusanov on 2016-9-27.
 */

public class MenuContent {

    int ID;
    int MenuId;
    int FoodId;

    public MenuContent(int ID, int menuId, int foodId) {
        this.ID = ID;
        MenuId = menuId;
        FoodId = foodId;
    }
    public MenuContent(int menuId, int foodId) {
        MenuId = menuId;
        FoodId = foodId;
    }
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getMenuId() {
        return MenuId;
    }

    public void setMenuId(int menuId) {
        MenuId = menuId;
    }

    public int getFoodId() {
        return FoodId;
    }

    public void setFoodId(int foodId) {
        FoodId = foodId;
    }
}
