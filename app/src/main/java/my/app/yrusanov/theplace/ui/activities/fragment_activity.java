package my.app.yrusanov.theplace.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.fragments.beer.Fragment_beer_categories;
import my.app.yrusanov.theplace.ui.fragments.gallery.Fragment_gallery_album;
import my.app.yrusanov.theplace.ui.fragments.lunch.Fragment_lunch_date_menu;
import my.app.yrusanov.theplace.ui.fragments.menu.Fragment_menu_categories;

import my.app.yrusanov.theplace.ui.fragments.news.Fragment_news;

public class fragment_activity extends AppCompatActivity {

    Fragment fragment;
    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        overridePendingTransition(R.animator.enter_from_left, R.animator.exit_to_right);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);

        //getActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String activityName;

        activityName = intent.getStringExtra("activity");

        if(activityName.equals("Beers_categories")){
            fragment = new Fragment_beer_categories();
        }
        else if(activityName.equals("Gallery_albums")){
            fragment = new Fragment_gallery_album();
        }
        else if(activityName.equals("Menu_categories")){
            fragment = new Fragment_menu_categories();
        }
        else if(activityName.equals("Lunch_menu")){
            //fragment = new Fragment_lunch_menu();
            fragment = new Fragment_lunch_date_menu();
        }
        else if(activityName.equals("News_type")){
            //fragment = new Fragment_lunch_menu();
            fragment = new Fragment_news();
        }


        fragmentTransaction = fragmentManager.beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left,R.animator.enter_from_left, R.animator.exit_to_right);
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        //fragmentTransaction.addToBackStack("fragment_activity");
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar'circular_progress_view Up/Home button
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                if(fragmentManager.getBackStackEntryCount() == 0)
                    this.onBackPressed();
                else
                    fragmentManager.popBackStack();
                //Toast.makeText(this,"test " + , Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
