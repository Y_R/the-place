package my.app.yrusanov.theplace.ui.adapters.Beers;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.DBOperation;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.DB.Types;
import my.app.yrusanov.theplace.ui.tables.BeerCategory;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-8-31.
 */
public class RecyclerAdapterBeersCategories extends RecyclerView.Adapter<RecyclerAdapterBeersCategories.ViewHolder> {

    private ArrayList<BeerCategory> elements;
    OnItemClickListener mItemClickListener;
    BeerCategory beerCategory;
    ContentResolver resolver;
    String path;
    public String language;
    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME+"");

    public RecyclerAdapterBeersCategories(Context _context){

        elements = new ArrayList<>();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(_context);
        language  = prefs.getString("list_preferences","en");
        resolver = _context.getContentResolver();
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/BeerCategories";
        String[] projection = new String[]{"ID", "NAME"};
        Cursor cursor = resolver.query(CONTENT_URL,projection,null,null,null);
        String contentName;
        if(cursor != null){
            cursor.moveToFirst();
            do{
                File imageFile;
                imageFile = new File(path + "/"+cursor.getString(1)+".jpg");
                contentName = DBOperation.getTranslation(_context, cursor.getInt(0), Types.BEERCATEGORIES,language,"Name");
                if(contentName.equals("")) {
                    contentName = cursor.getString(1);
                }

                if(imageFile.exists()){
                    Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                    elements.add(new BeerCategory(cursor.getInt(0),contentName, bitmap));
                }
                else {
                    Bitmap bitmap = BitmapFactory.decodeResource(_context.getResources(),R.drawable.beer);
                    elements.add(new BeerCategory(cursor.getInt(0),contentName, bitmap));
                }
            }while (cursor.moveToNext());
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_beer_categories,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        beerCategory = elements.get(position);
        holder.textView_beer_IdCategory.setText(String.valueOf(beerCategory.getID()));
        holder.textView_beer_category.setText(beerCategory.getName());
        holder.imageViewBeerCategory.setImageBitmap(beerCategory.getImage());
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        TextView textView_beer_category, textView_beer_IdCategory;
        ImageView imageViewBeerCategory;
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView_beer_category = (TextView)itemView.findViewById(R.id.textView_beer_categories);
            textView_beer_IdCategory = (TextView)itemView.findViewById(R.id.textView_beer_IdCategory);
            imageViewBeerCategory = (ImageView)itemView.findViewById(R.id.imageViewBeerCategory);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface  OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener)
    {
        this.mItemClickListener = mItemClickListener;
    }
}
