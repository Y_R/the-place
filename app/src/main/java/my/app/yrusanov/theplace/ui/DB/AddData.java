package my.app.yrusanov.theplace.ui.DB;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox;

/**
 * Created by y.rusanov on 2017-7-13.
 */

public class AddData {

    private ContentResolver provider;

    public AddData(Context _context) {
        provider = _context.getContentResolver();
        this.insertData();
    }

    public void insertData(){

        this.insertBeerCategories();
        this.insertBeers();
        this.insertAlbums();
        this.insertImage();
        this.insertFoodCategories();
        this.insertFoods();
        this.insertNewsType();
        this.insertNews();

    }


    private void insertBeers(){
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_BEER;

        ContentValues values = new ContentValues();

        values.put(Fields.ID, 1);
        values.put(Fields.NAME, "Samuel Adams Winter Lager");
        values.put(Fields.WEIGHT, "500");
        values.put(Fields.ALCOHOL, "6.90%");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 1);
        values.put(Fields.COUNTRY, "Texas");
        values.put(Fields.STYLE, "Bock");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 2);
        values.put(Fields.NAME, "Saint Arnold Spring Bock");
        values.put(Fields.WEIGHT, "500");
        values.put(Fields.ALCOHOL, "6.90%");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 1);
        values.put(Fields.COUNTRY, "Texas");
        values.put(Fields.STYLE, "Bock");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 3);
        values.put(Fields.NAME, "Schell's Bock");
        values.put(Fields.WEIGHT, "500");
        values.put(Fields.ALCOHOL, "6.90%");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 1);
        values.put(Fields.COUNTRY, "Texas");
        values.put(Fields.STYLE, "Bock");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 4);
        values.put(Fields.NAME, "Hofbräu Dunkel");
        values.put(Fields.WEIGHT, "500");
        values.put(Fields.ALCOHOL, "5.50%");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 2);
        values.put(Fields.COUNTRY, "Germany");
        values.put(Fields.STYLE, "Dunkel");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 5);
        values.put(Fields.NAME, "Lobo Negro");
        values.put(Fields.WEIGHT, "500");
        values.put(Fields.ALCOHOL, "5.90%");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 2);
        values.put(Fields.COUNTRY, "Germany");
        values.put(Fields.STYLE, "Dunkel");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 6);
        values.put(Fields.NAME, "Fearless Youth");
        values.put(Fields.WEIGHT, "500");
        values.put(Fields.ALCOHOL, "6.90%");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 2);
        values.put(Fields.COUNTRY, "Germany");
        values.put(Fields.STYLE, "Dunkel");
        provider.insert(CONTENT_URL, values);


        values.put(Fields.ID, 7);
        values.put(Fields.NAME, "Anchor Liberty Ale");
        values.put(Fields.WEIGHT, "500");
        values.put(Fields.ALCOHOL, "5.50%");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 3);
        values.put(Fields.COUNTRY, "Massachusetts");
        values.put(Fields.STYLE, "India pale ale");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 8);
        values.put(Fields.NAME, "BrewDog Punk");
        values.put(Fields.WEIGHT, "500");
        values.put(Fields.ALCOHOL, "5.90%");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 3);
        values.put(Fields.COUNTRY, "Massachusetts");
        values.put(Fields.STYLE, "India pale ale");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 9);
        values.put(Fields.NAME, "BrewDog Dead Pony");
        values.put(Fields.WEIGHT, "500");
        values.put(Fields.ALCOHOL, "6.90%");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 3);
        values.put(Fields.COUNTRY, "Massachusetts");
        values.put(Fields.STYLE, "India pale ale");
        provider.insert(CONTENT_URL, values);





    }

    private void insertBeerCategories(){
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_BEERCATEGPRY;

        ContentValues values = new ContentValues();

        values.put(Fields.ID, 1);
        values.put(Fields.NAME, "Bock");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 2);
        values.put(Fields.NAME, "Dunkel");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 3);
        values.put(Fields.NAME, "India pale ale");
        provider.insert(CONTENT_URL, values);
    }


    private void insertFoods(){
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_FOOD;

        ContentValues values = new ContentValues();

        values.put(Fields.ID, 1);
        values.put(Fields.NAME, "SUMMER SALAD");
        values.put(Fields.WEIGHT, "Peeled tomatoes, roasted peppers, cheese, olive oil.");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 1);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 2);
        values.put(Fields.NAME, "ENERGY SALAD");
        values.put(Fields.WEIGHT, "Eincorn, imperial rice, quinoa, spinach, tomatoes, parsley, vegetable dressing");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 1);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 3);
        values.put(Fields.NAME, "CAESAR SALAD WITH CHICKEN");
        values.put(Fields.WEIGHT, "Lettuce, yellow cheese, croutons, crispy chicken, parmesan, anchovy Ceaser sause.");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 1);
        provider.insert(CONTENT_URL, values);


        values.put(Fields.ID, 4);
        values.put(Fields.NAME, "HAPPY BEEF BURGER");
        values.put(Fields.WEIGHT, "Bread with sesame, beef burger, cheddar, bacon strips, fried onions, honey mustard sauce, french fries.");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 2);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 5);
        values.put(Fields.NAME, "PARADISE BURGER");
        values.put(Fields.WEIGHT, "Sesame roll, slow cooked choppy pork, cheddar cheese, stewed onions, pickles and barbecue sauce. Garnish with fries.");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 2);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 6);
        values.put(Fields.NAME, "CRISPY BURGER");
        values.put(Fields.WEIGHT, "Bread with sesame, crispy chicken steak, Swedish mix sauce, tomato, iceberg, cheddar, french fries.");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 2);
        provider.insert(CONTENT_URL, values);


        values.put(Fields.ID, 7);
        values.put(Fields.NAME, "SALMON WITH QUINOA");
        values.put(Fields.WEIGHT, "Fillet salmon, cinnama, green salad, peeled tomatoes and olives.");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 3);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 8);
        values.put(Fields.NAME, "FISH FILLETS IN PARMESAN");
        values.put(Fields.WEIGHT, "White fish pangasius in parmesan and parsley breading, roasted seasoned potatoes, special sauce.");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 3);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 9);
        values.put(Fields.NAME, "FISH A LA CREME");
        values.put(Fields.WEIGHT, "White fish fillets, baked potatoes, \"Alfredo\" sauce with onion and garlic.");
        values.put(Fields.PRICE, 0);
        values.put(Fields.IDCATEGORY, 3);
        provider.insert(CONTENT_URL, values);

    }

    private void insertFoodCategories(){
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_FOODCATEGORY;

        ContentValues values = new ContentValues();

        values.put(Fields.ID, 1);
        values.put(Fields.NAME, "SALADS");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 2);
        values.put(Fields.NAME, "BURGERS");
        provider.insert(CONTENT_URL, values);


        values.put(Fields.ID, 3);
        values.put(Fields.NAME, "FISH");
        provider.insert(CONTENT_URL, values);

    }


    private void insertAlbums(){
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_ALBUMS;

        ContentValues values = new ContentValues();

        values.put(Fields.ID, 1);
        values.put(Fields.NAME, "Gallery 1");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 2);
        values.put(Fields.NAME, "Gallery 2");
        provider.insert(CONTENT_URL, values);

    }

    private void insertImage(){
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_IMAGES;

        ContentValues values = new ContentValues();

        values.put(Fields.ID, 1);
        values.put(Fields.NAME, "Image 1");
        values.put(Fields.IDALBUM, 1);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 2);
        values.put(Fields.NAME, "Image 2");
        values.put(Fields.IDALBUM, 1);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 3);
        values.put(Fields.NAME, "Image 3");
        values.put(Fields.IDALBUM, 2);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 4);
        values.put(Fields.NAME, "Image 4");
        values.put(Fields.IDALBUM, 2);
        provider.insert(CONTENT_URL, values);
    }

    private void insertNewsType(){
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_NEWS_TYPE;

        ContentValues values = new ContentValues();

        values.put(Fields.ID, 1);
        values.put(Fields.NAME, "New Beer");
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 2);
        values.put(Fields.NAME, "New Salad");
        provider.insert(CONTENT_URL, values);
    }

    private void insertNews(){
        final Uri CONTENT_URL = ContentProviderBeerBox.CONTENT_URI_NEWS;

        ContentValues values = new ContentValues();

        values.put(Fields.ID, 1);
        values.put(Fields.NAME, "American Amber Ale");
        values.put(Fields.DESCRIPTION, "American-style amber ales have medium-high to high maltiness with medium to low caramel character.");
        values.put(Fields.IDNEWS, 1);
        provider.insert(CONTENT_URL, values);

        values.put(Fields.ID, 2);
        values.put(Fields.NAME, "SALAD EINKORN WITH AVOCADO");
        values.put(Fields.DESCRIPTION, "Einkorn, peeled tomatoes, arugula, avocado, balsamic soy - mustard dressing.");
        values.put(Fields.IDNEWS, 2);
        provider.insert(CONTENT_URL, values);
    }
}
