package my.app.yrusanov.theplace.ui.tables;

/**
 * Created by y.rusanov on 2017-2-28.
 */

public class ItemTranslation {

    private int id;
    private int idItemTranslation;
    private String type;
    private String languageId;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public ItemTranslation(int _id, int _idItemTranslation, String _type, String _name, String _languageId){
        this.id = _id;
        this.idItemTranslation = _idItemTranslation;
        this.type = _type;
        this.languageId = _languageId;
        this.name = _name;
    }
    public ItemTranslation(int _id, int _idItemTranslation, String _type, String _name, String _languageId, String _description){
        this.id = _id;
        this.idItemTranslation = _idItemTranslation;
        this.type = _type;
        this.languageId = _languageId;
        this.name = _name;
        this.description = _description;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdItemTranslation() {
        return idItemTranslation;
    }

    public void setIdItemTranslation(int idItemTranslation) {
        this.idItemTranslation = idItemTranslation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }
}
