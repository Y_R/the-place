package my.app.yrusanov.theplace.ui.DB;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Created by YR on 10/8/2016.
 */

public class DBOperation extends DBHelper {


    private String table;

    public DBOperation(Context context, String _table) {
        super(context, _table);
        this.table = _table;
    }

    public void insertData(Map<String, ?> _field) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Set keys = _field.keySet();
        StringBuilder stringBuilder = new StringBuilder(256);
        String comm = "";
        stringBuilder.append("INSERT INTO ").append(table).append(" (");
        for (Object key : keys) {

            stringBuilder.append(comm).append(key.toString().toUpperCase());
            comm = ", ";
        }
        stringBuilder.append(" ) VALUES (");
        comm = " ";
        keys = _field.keySet();
        for (Object key : keys) {

            if (String.class.isInstance(_field.get(key))) {
                stringBuilder.append(comm).append(this.escapeStr((String) _field.get(key)));
            } else if (Integer.class.isInstance(_field.get(key))) {
                stringBuilder.append(comm).append((Integer) _field.get(key));
            } else if (Boolean.class.isInstance(_field.get(key))) {
                stringBuilder.append(comm).append((Boolean) _field.get(key));
            } else if (Double.class.isInstance(_field.get(key))) {
                stringBuilder.append(comm).append((Double) _field.get(key));
            } else if (Date.class.isInstance(_field.get(key))) {
                stringBuilder.append(comm).append((String) _field.get(key)).append("00:00:00");
            } else if (Float.class.isInstance(_field.get(key))) {
                stringBuilder.append(comm).append((Float) _field.get(key));
            }
            comm = ", ";
        }
        stringBuilder.append(")");
        db.execSQL(stringBuilder.toString());
        db.close();
    }

    private Object setObjectValue(Object _value) {
        if (String.class.isInstance(_value)) {
            return (String) _value;
        } else if (Integer.class.isInstance(_value)) {
            return (Integer) _value;
        } else if (Boolean.class.isInstance(_value)) {
            return (Boolean) _value;
        } else if (Double.class.isInstance(_value)) {
            return (Double) _value;
        } else
            return null;
    }

    private String escapeStr(String _value) {
        return "'" + _value + "'";
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = null;
        String query = "SELECT * FROM " + table;
        res = db.rawQuery(query, null);
        res.moveToFirst();
        db.close();
        return res;
    }

    public void deleteRecords() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(table, "", null);
        db.close();
    }

    public Cursor getDataByCategory(int _IdCategory, String _columnEq) {
        Cursor res = db.rawQuery("SELECT * FROM " + table + " WHERE " + _columnEq + " = " + _IdCategory, null);
        res.moveToFirst();
        return res;
    }


    public int getLastRecordId() {
        Cursor res = db.rawQuery("SELECT * FROM " + table + " ORDER BY " + Fields.ID + " DESC", null);
        res.moveToFirst();
        int lastRecId;
        if (res.getCount() == 0)
            lastRecId = 0;
        else
            lastRecId = res.getInt(0);

        res.close();
        return lastRecId;
    }

    public boolean checkDataExists(String _nameColumn, Object _value) {

        String query;

        if (this.setObjectValue(_value) instanceof String) {
            query = "SELECT * FROM " + table + " WHERE " + _nameColumn + " = '" + this.setObjectValue(_value) + "'";
        } else {
            query = "SELECT * FROM " + table + " WHERE " + _nameColumn + " = " + this.setObjectValue(_value) + "";
        }

        Cursor res = db.rawQuery(query, null);
        res.moveToFirst();
        if (res.getCount() == -1 || res.getCount() == 0)
            return false;
        else
            return true;
    }

    public static String getTranslation(Context _context, int _ItemId, String _type, String _language, String _getNameDescription) {
        ContentResolver resolver;
        resolver = _context.getContentResolver();
        Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/" + Tables.TABLE_NAME_ITEMTRANSLATION + "");
        String whereClause = "" + Fields.IDITEMTRANSLATION + " = " + _ItemId + " AND " + Fields.LANGUAGEID + " = '" + _language + "' AND "+Fields.TYPE+" = '"+_type+"'";
        Cursor cursor = resolver.query(CONTENT_URL, null, whereClause, null, null);
        String content = "";

        if (cursor != null) {
            cursor.moveToFirst();
            if (_getNameDescription.equals("Description")) {
                content = cursor.getString(2);
            } else {
                content = cursor.getString(2);
            }
        }

        return content;
    }
}
