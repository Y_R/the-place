package my.app.yrusanov.theplace.ui.fragments.gallery;


import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Fields;
import my.app.yrusanov.theplace.ui.DB.Tables;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_image extends Fragment {

    static final Uri CONTENT_URL = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_IMAGES+"");
    String path;
    ContentResolver resolver;
    TextView textViewName;
    ImageView imageView;
    public Fragment_image() {
        // Required empty public constructor
    }


    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Toast.makeText(container.getContext(),"Image select", Toast.LENGTH_SHORT).show();
        view = inflater.inflate(R.layout.fragment_image, container, false);

        Bundle bundle = this.getArguments();
        resolver = view.getContext().getContentResolver();
        textViewName = (TextView)view.findViewById(R.id.textViewName_image);
        imageView = (ImageView)view.findViewById(R.id.imageView_image);
        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/Images";

        String name = bundle.getString("Name");

        String whereClause = ""+ Fields.NAME+" = '"+name+"'";
        Cursor cursor = resolver.query(CONTENT_URL,null,whereClause,null,null);
        cursor.moveToFirst();
        File imageFile;
        imageFile = new File(path + "/"+cursor.getString(1)+".jpg");
        textViewName.setText(cursor.getString(1));
        if(imageFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            imageView.setImageBitmap(bitmap);
        }

        return view;
    }
}
