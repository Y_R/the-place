package my.app.yrusanov.theplace.ui.activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import my.app.yrusanov.theplace.R;
import my.app.yrusanov.theplace.ui.DB.Tables;
import my.app.yrusanov.theplace.ui.adapters.Lunch.RecyclerAdapterLunch;

import java.text.ParseException;

public class Lunch extends AppCompatActivity {


    RecyclerView recyclerView;
    ContentResolver resolver;
    LinearLayoutManager mLayoutManager;
    RecyclerAdapterLunch adapter;
    static final Uri CONTENT_URL_MENU = Uri.parse("content://my.app.yrusanov.theplace.ui.contentProvider.ContentProviderBeerBox/"+ Tables.TABLE_NAME_MENU+"");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lunch);
        getSupportActionBar().setTitle(R.string.LunchMenu);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_lunch_menu);
        mLayoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(mLayoutManager);

        try {
            adapter = new RecyclerAdapterLunch(this);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        recyclerView.setAdapter(adapter);

        adapter.SetOnItemClickListener(new RecyclerAdapterLunch.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                TextView textView = (TextView) view.findViewById(R.id.textViewIdMenu);
                TextView textView1 = (TextView)view.findViewById(R.id.textView_lunch_menu_date);
                TextView textView2 = (TextView)view.findViewById(R.id.textView_lunch_menu_day);
                Intent intent = new Intent(view.getContext(), fragment_activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("Id",textView.getText().toString());
                bundle.putString("DateLunch", (String) textView1.getText());
                bundle.putString("DayLunch",(String) textView2.getText());
                resolver = getContentResolver();

                intent.putExtra("activity", "Lunch_menu");
                intent.putExtra("Id",textView.getText().toString());
                intent.putExtra("DateLunch", (String) textView1.getText());
                intent.putExtra("DayLunch",(String) textView2.getText());
                startActivity(intent);
                overridePendingTransition(R.animator.enter_from_right, R.animator.exit_to_left);

                /*
                fragment_lunch_date_menu.setArguments(bundle);

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.animator.enter_from_right, R.animator.exit_to_left,R.animator.enter_from_left, R.animator.exit_to_right);
                //fragmentTransaction.setCustomAnimations(R.animator.fadein, R.animator.fadeout);
                fragmentTransaction.replace(R.id.fragment_container, fragment_lunch_date_menu);
                fragmentTransaction.addToBackStack("fragment_lunch_menu");
                fragmentTransaction.commit();
                */
            }
        });
    }

    public void clickLunch_lunch(View v){
        Intent intent = new Intent(v.getContext(), fragment_activity.class);
        intent.putExtra("activity", "Lunch_menu");
        startActivity(intent);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
