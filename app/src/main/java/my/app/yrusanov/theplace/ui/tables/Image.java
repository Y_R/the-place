package my.app.yrusanov.theplace.ui.tables;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by y.rusanov on 2016-9-7.
 */
public class Image {

    private int ID;
    private String Name;
    private String Description;
    private Bitmap Image;
    private int IDAlbum;
    private ArrayList<Image> imageArrayList;


    public Image(int ID, String name, String description, Bitmap image, int IDAlbum) {
        this.ID = ID;
        this.Name = name;
        this.Description = description;
        this.Image = image;
        this.IDAlbum = IDAlbum;
    }

    public Image(int ID, String name, String description, int IDAlbum) {
        this.ID = ID;
        this.Name = name;
        this.Description = description;
        this.IDAlbum = IDAlbum;
    }

    public Image(String name, String description, int IDAlbum){
        this.Name = name;
        this.Description = description;
        this.IDAlbum = IDAlbum;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }

    public int getIDAlbum() {
        return IDAlbum;
    }

    public void setIDAlbum(int IDAlbum) {
        this.IDAlbum = IDAlbum;
    }

    public ArrayList<Image> getImageArrayList() {
        return imageArrayList;
    }

    public void setImageArrayList(ArrayList<?> imageArrayList) {

        ArrayList<Image> localImageArrayList = new ArrayList<>();
        ArrayList<Object> localArrayList;

        for(int i = 0; i < imageArrayList.size(); i++){
            localArrayList = (ArrayList<Object>) imageArrayList.get(i);

            localImageArrayList.add(new Image(
                    Integer.parseInt((String) localArrayList.get(0)),
                    (String) localArrayList.get(1),
                    (String) localArrayList.get(2),
                    (Bitmap) localArrayList.get(3),
                    Integer.parseInt((String) localArrayList.get(4))
            ));
        }

        this.imageArrayList = localImageArrayList;
    }

}
