package my.app.yrusanov.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import my.app.yrusanov.theplace.ui.DB.Types;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by y.rusanov on 2017-1-28.
 */

public class ImagesFile {

    Context context;
    Map<String, byte[]> mapValue;
    private File file;
    private String path;

    public ImagesFile(Context _context, Map<String, byte[]> _mapValue, String _typeImage){


        if(_typeImage.equals(Types.BEER)){
            path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/"+Types.BEER+"";
        }
        else if(_typeImage.equals(Types.FOOD)){
            path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/"+Types.FOOD+"";
        }
        else if(_typeImage.equals(Types.IMAGES)){
            path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/"+Types.IMAGES+"";
        }
        else if(_typeImage.equals(Types.BEERCATEGORIES)){
            path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/"+Types.BEERCATEGORIES+"";
        }
        else if(_typeImage.equals(Types.FOODCATEGORIES)){
            path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/"+Types.FOODCATEGORIES+"";
        }
        else if(_typeImage.equals(Types.NEWS)){
            path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/"+Types.NEWS+"";
        }

        context = _context;
        mapValue = _mapValue;
        final File dir = new  File(path);
        boolean isDirectoryCreated=dir.exists();
        if (!isDirectoryCreated) {
            isDirectoryCreated= dir.mkdirs();
            if(!isDirectoryCreated){
                isDirectoryCreated= dir.mkdir();
            }
        }
        if(isDirectoryCreated) {
            Set keys = mapValue.keySet();

            for(Object key : keys) {

                File root = context.getExternalCacheDir();

                file  = new File(path + "/"+(String)key+".jpg");

                if(file.exists())
                    System.out.println("File "+key+" exists"); //file.delete();
                else{
                    FileOutputStream fos = null;
                    try{
                        System.out.print("Yuri Image: "+ root +";" + file.getPath() + ";"+mapValue.get(key).toString());
                        byte[] imageByte = mapValue.get(key);
                        if(imageByte.length > 0){
                            fos = new FileOutputStream(file.getPath());
                            fos.write(imageByte);
                            fos.close();
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    public String getFilePath(){
        return file.getPath();
    }

    public static Bitmap getImageByType(String _name, String _type){
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BeerBox/"+_type+"";
        File imageFile;
        imageFile = new File(path + "/"+_name+".jpg");
        if(imageFile.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            return bitmap;
        }
        return null;
    }
}
